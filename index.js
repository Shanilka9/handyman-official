/** @format */
import React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';

const Application = () => (
        <App />
);




AppRegistry.registerComponent('HandyMan', () => Application);
