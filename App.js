/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet } from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import { View } from 'native-base';

/*-----------------------------------------------------------------------------------*/

import SignUp from './src/components/SignUp';
import SignIn from './src/components/SignIn';
import MapScreen from './src/components/MapScreen';
import ServiceArea from './src/components/ServiceArea';
import ServiceDetails from './src/components/ServiceDetails';
import OrderSummery from './src/components/OrderSummery';
import NewDash from './src/components/NewDashboard';
import ServiceIssue from './src/components/ServiceIssue';
import Verification from './src/components/Verification';
import Authenticator from './src/components/Authenticator';
import ProviderRequest from './src/components/ProviderRequest';

import MyOrderList from './src/components/MyOrderList/MyOrderList';

import About from './src/components/SideBar/Slide_About';
import OperationAndPricing from './src/components/SideBar/Slide_OperationAndPricing';
import StrategicPartner from './src/components/SideBar/Slide_StrategicPartner';

import FPWEnterUserName from './src/components/ForgetPassword/FPW_EnterUserName';
import FPWEnterPinCode from './src/components/ForgetPassword/FPW_EnterPinCode';
import FPWResetPW from './src/components/ForgetPassword/FPW_ResetPassword';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};

const RootStack = createStackNavigator(
  {

    NewDash: {
      screen: NewDash,
      navigationOptions: { header: null }

    },

    About: {
      screen: About,
      navigationOptions: { header: null }

    },

    Pricing: {
      screen: OperationAndPricing,
      navigationOptions: { header: null }

    },

    Partner: {
      screen: StrategicPartner,
      navigationOptions: { header: null }

    },
    
    Issue: {
      screen: ServiceIssue,
      navigationOptions: { header: null }

    },

    OrderList: {
      screen: MyOrderList,
      navigationOptions: { header: null }

    },

    MyOrder: {
      screen: MyOrderList,
      navigationOptions: { header: null }

    },

    SignUp: {
      screen: SignUp,
      navigationOptions: { header: null }

    },

    SignIn: {
      screen: SignIn,
      navigationOptions: { header: null }

    },

    Area: {
      screen: ServiceArea,
      navigationOptions: { header: null }

    },

    Map: {
      screen: MapScreen,
      navigationOptions: { header: null }

    },

    Details: {
      screen: ServiceDetails,
      navigationOptions: { header: null }

    },

    Summery: {
      screen: OrderSummery,
      navigationOptions: { header: null }

    },
    Verify: {
      screen: Verification,
      navigationOptions: { header: null }

    },

    Auth: {
      screen: Authenticator,
      navigationOptions: { header: null }
    },

    Provider: {
      screen: ProviderRequest,
      navigationOptions: { header: null }
    },

    FPW_EnterUserName: {
      screen: FPWEnterUserName,
      navigationOptions: { header: null }
    },

    FPW_EnterPin: {
      screen: FPWEnterPinCode,
      navigationOptions: { header: null }
    },

    FPW_ResetPW: {
      screen: FPWResetPW,
      navigationOptions: { header: null }
    },

  },
  {
    initialRouteName: 'Auth',
  },
  {
    headerMode: 'screen'
  }

);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component<Props> {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppContainer />
        <FlashMessage position="top" />
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
