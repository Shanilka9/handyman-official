
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert,
    AsyncStorage,
    NetInfo
} from 'react-native';
import { Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

export default class SignIn extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullName: '',
            username: '',
            email: '',
            password: '',
            validated: false,
            clientData: null,
            place: "LOGIN"
        }
    }


    // refresh() {
    //     this.setState({
    //         place: 'LOGIN'
    //     });
    // }

    // handleBackPress = () => {
    //     if (this.state.place == 'LOGIN') {
    //         BackHandler.exitApp();
    //         console.log("EXIT")

    //     } else {
    //         this.props.navigation.goBack()
    //     }
    //     console.log("BACKPRESS")
    //     return true;
    // }

    // loginErrorMsg() {
    //     // declare error message infromation
    //     showMessage({
    //         message: "Login Failed",
    //         description: "Username and Password is Incorrect !",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "black", // text color
    //     });
    // }


    // connectionError() {
    //     // declare error message infromation
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "Please check your Network Connection !",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    // signUpError() {
    //     showMessage({
    //         message: "Signup In Progress",
    //         description: "Verify Your Phone Before Signin",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    onClickListener = (viewId) => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BaseUrl+'api/app/client/login?username=' + this.state.username + '&password=' + this.state.password, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    }
                })

                    .then((response) => response.json())
                    .then((responseJson) => {
                        if (responseJson != null) {
                            this.setState({
                                clientData: responseJson
                            });
                            // this._storeData();
                            this.verificationCheck();
                            // console.log("Login : " + JSON.stringify(responseJson));
                            // console.log("Login2 : " + JSON.stringify(this.state.clientData.isVerify));
                        } else {
                            // Alert.alert("Login Failed", "Username and Password is Incorrect !");
                            Alerts.FlashMessage(Strings.LoginFailedAlert,Strings.LoginFailedAlertDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.AlertTextColor)
                        }
                    })

                    .catch((error) => {
                        // Alert.alert("Login Failed", "Username and Password is Incorrect !");
                        //this.loginErrorMsg();
                        Alerts.FlashMessage(Strings.LoginFailedAlert,Strings.LoginFailedAlertDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.AlertTextColor)
                    });
            } else {
                //this.connectionError();
                // Alert.alert("Connection Failed", "Please check your Network Connection !");
                Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
            }
        });
    }

    verificationCheck() {
        if (this.state.clientData.isVerify == true) {
            this._storeData();
        } else {
            // Alert.alert("Signup In Progress", "Verify Your Phone Before Signin");
            this.signUpError();
            this.props.navigation.navigate('Verify', {
                navAppData: this.state.clientData
            });
        }
    }


    _storeData = async () => {
        try {
            await AsyncStorage.setItem('client_data', JSON.stringify(this.state.clientData), () => {
                // Alert.alert("Login Success");
                this.props.navigation.navigate('NewDash');
            });

        } catch (error) {
            console.error(error);
        }
    }



    render() {
        return (
            <ScrollView style={styles.scrollStyle} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.container}>
                    <Image source={require('../assets/handy.png')} style={styles.HeadericonlImage2} />

                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={require('../assets/avatar.png')} />
                        <TextInput style={styles.inputs}
                            placeholder="User Name"
                            value={this.setState.username}
                            keyboardType="email-address"
                            underlineColorAndroid='transparent'
                            onChangeText={(username) => this.setState({ username })}
                        />
                    </View>

                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={require('../assets/keyIcon.png')} />
                        <TextInput style={styles.inputs}
                            autoCapitalize="none" autoCorrect={false}
                            value={this.setState.password}
                            placeholder="Password"
                            secureTextEntry={true}
                            underlineColorAndroid='transparent'
                            onChangeText={(password) => this.setState({ password })}

                        />
                    </View>

                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.onClickListener('login')}>
                        <Text style={styles.signUpText}>Sign in</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.props.navigation.navigate('SignUp')}>
                        <Text style={styles.signUpText}>Sign up</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={styles.FPBContainer} onPress={() => this.props.navigation.navigate('FPW_EnterUserName')} >
                        <Text style={styles.signUpText}>Forgot Password ?</Text>
                    </TouchableHighlight>




                </View>
            </ ScrollView>



        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },
    scrollStyle: {
        // backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        width: 310,
        height: 150,
        marginBottom: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#848181',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#00b5ec',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },

    FPBContainer: {
        height: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 20,

        // color: 'black'



    },

    FPButton: {
        backgroundColor: "#f4c741",
    },


    signupButton: {
        backgroundColor: "#f4c741",
    },
    signUpText: {
        color: 'black',
        fontSize: 15,

    }
});