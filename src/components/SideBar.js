import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  NetInfo,
  ActivityIndicator,
  Picker,
  Linking,
  AsyncStorage
} from 'react-native';

import { ScrollView } from 'react-native-gesture-handler';
import { Container, Header, Content, Button, Icon, Card, CardItem, Body, } from 'native-base';
import PropTypes from 'prop-types';

class SideBar extends Component {

  constructor(props) {
    super(props);
  }

  navigateToScreen(route) {
    if (route === 'about') {
      this.props.navigation.navigate("About");
    } else if (route === 'pricing') {
      this.props.navigation.navigate("Pricing");
    } else if (route === 'partner') {
      this.props.navigation.navigate("Partner");
    } else if (route === 'logout') {
      Linking.openURL('https://handyman.qa/')

    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ marginTop: 20 }}>

            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
              <Image source={require('../assets/handy.png')} style={styles.HeaderIconlImage2} />
            </View>

            <View style={{ marginTop: 40 }}>
              <Button iconLeft full bordered style={{ margin: 10, borderRadius: 100, borderColor: '#fcbf19', backgroundColor: '#fcbf19', justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('about')}>
                <Icon name='home' style={{ color: '#59554a' }} />
                <Text style={{ color: 'white', marginRight: 18, fontSize: 16, fontWeight: '500' }} >&nbsp;&nbsp;&nbsp;About</Text>
              </Button>

              <Button iconLeft full bordered style={{ margin: 10, borderRadius: 100, borderColor: '#fcbf19', backgroundColor: '#fcbf19', justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('pricing')}>
                <Icon name='md-list' style={{ color: '#59554a' }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp; Operation & Pricing </Text>
              </Button>

              <Button iconLeft full bordered style={{ margin: 10, borderRadius: 100, borderColor: '#fcbf19', backgroundColor: '#fcbf19', justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('partner')}>
                <Icon name='md-briefcase' style={{ color: '#59554a' }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp;Our Strategic Partner</Text>
              </Button>



              <Card transparent style={styles.cardStyle} >
                <CardItem bordered>
                  <Body>
                    <Text style={styles.slideHeader}>
                      Contact Us
                  </Text>
                  </Body>
                </CardItem>

                <CardItem bordered>
                  <Body style={styles.dataStyle} >
                    <Text  style={styles.textStyle}>
                      Our Call Center : +9744434643 
                  </Text>

                    <Text style={styles.textStyle}>
                      Hotline : +9745639000
                  </Text>

                  </Body>
                </CardItem>

                <CardItem bordered>
                  <Body>
                    <Text style={styles.slideHeader2}>
                      Email
                  </Text>
                  </Body>
                </CardItem>

                <CardItem bordered>
                  <Body style={styles.dataStyle} >
                    <Text style={styles.textStyle}>
                      info@handyman.qa
                  </Text>

                    <Text style={styles.textStyle}>
                      Direct Trading & Contracting
                  </Text>

                  </Body>
                </CardItem>

              </Card>



            </View>




          </View>
        </ScrollView>

        <View style={{marginBottom: 10}}>
          <Button iconLeft full bordered warning style={{ margin: 10, borderRadius: 100, borderColor: '#fcbf19', backgroundColor: '#fcbf19',justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('logout')}>
            <Icon name='arrow-back' style={{ color: '#59554a', marginRight: 10 }} />
            <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight:'500' }}>&nbsp;Visit Handyman.qa</Text>
          </Button>


        </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    paddingTop: 6,
    paddingRight: 1,
    flex: 1,
    // backgroundColor: '#f0ad4e'
  },

  cardStyle: {
    borderRadius: 20,


  },

  textStyle:{
    fontWeight: 'normal',
    fontSize: 15,
    fontWeight:'200'


  },

  slideHeader: {

    flex: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',

    marginLeft: 95,
    color: '#fcbf19',
    fontWeight: 'bold',
    fontSize: 16

  },



  slideHeader2: {
    flex: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',

    marginLeft: 110,
    color: '#fcbf19',
    fontWeight: 'bold',
    fontSize: 16

  },

  dataStyle: {
    flex: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    // marginLeft: 10
  },

  navItemStyle: {
    padding: 10
  },
  navSectionStyle: {
    backgroundColor: 'lightgrey'
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  footerContainer: {
    padding: 20,
    backgroundColor: 'lightgrey'
  },
  HeaderIconlImage2: {
    width: 130,
    height: 62,
    marginTop: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
SideBar.propTypes = {
  navigation: PropTypes.object
};


export default SideBar;