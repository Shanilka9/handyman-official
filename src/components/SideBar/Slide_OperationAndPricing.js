import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View
} from 'react-native';

import {
    Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Button, Icon, Title
} from 'native-base';

export default class Slide_OperationAndPricing extends Component {

    constructor(props) {
        super(props);
        state = {

        }
    }

    render() {

        return (
            <Container>
                <Header style={styles.headerStyles}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>

                    <Body style={styles.bodyStyle}>
                        <Title style={styles.headerStyles}>Pricing</Title>
                    </Body>

                    <Right>
                        <Button transparent>
                            {/* <Text>Cancel</Text> */}
                        </Button>
                    </Right>
                </Header>


                <Content padder>

                    <Card transparent>

                        <CardItem>
                            <View style={styles.container}>
                                <Image transparent source={require('../../assets/handy.png')} style={styles.HeadericonlImage2} />
                            </View>
                        </CardItem>

                        <CardItem>
                            <Body>
                                <Text style={styles.dataStyle}>
                                    All customer requests received through Handyman Service Application
                                    are assigned to registerd service providers who have been screened for
                                    competency before appointing.

                                    {"\n"} {"\n"}

                                    Assigned Service Provider will then contact the enquirer and arrange timing to visit,
                                    discuss rates based on repairs / supplies and settle direct to Service Provider upon
                                    satisfactory completion of the service.

                                    {"\n"} {"\n"}

                                    At Handyman App we do not stop at just connecting customer and service provider.
                                    We follow up with Quality Audits to ensure quality services to our valued clientele.
                                </Text>
                            </Body>
                        </CardItem>

                    </Card>
                </Content>
            </Container>


        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
        // padding: 10
    },

    scrollStyle: {
        backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        padding: 10,
        width: 200,
        height: 95,
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center',

    },

    dataStyle: {
        textAlign: 'center',
        justifyContent: 'center',
        // marginLeft: 10
        fontSize: 15
    },

    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18
    },

    bodyStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

});



