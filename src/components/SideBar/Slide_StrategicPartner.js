import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View
} from 'react-native';

import {
    Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Button, Icon, Title
} from 'native-base';

export default class Slide_StrategicPartner extends Component {

    constructor(props) {
        super(props);
        state = {

        }
    }

    render() {

        return (
            <Container>
                <Header style={styles.headerStyles}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>

                    <Body style={styles.bodyStyle}>
                        <Title style={styles.headerStyles}>Partners</Title>
                    </Body>

                    <Right>
                        <Button transparent>
                            {/* <Text>Cancel</Text> */}
                        </Button>
                    </Right>
                </Header>


                <Content padder>

                    <Card transparent>
{/* 
                        <CardItem>
                            <View style={styles.container}>
                                <Image transparent source={require('../assets/handy.png')} style={styles.HeadericonlImage2} />
                            </View>
                        </CardItem> */}

                        <CardItem>
                            <Body>
                                <Text style={styles.dataStyle}>
                                    Handyman App is privilaged to be associated with Jumbo Electronics Qatar as the Strategic Partner
                               
                                    {"\n"} {"\n"}

                                    Jumbo Electronics Qatar, in their 38th year of operations in Qatar is undoubtedly their
                                    foremost Service Provider in the country and also having the largest electronics & home
                                    appliances service facility in Qatar.

                                    {"\n"} {"\n"}
                                
                                    Handyman App also offers a link to trusted brands marketed by Jumbo Electronics Qatar.
                                </Text>

                                

                            </Body>
                        </CardItem>

                    </Card>

                    <Text style={styles.dataStyle} style={{ fontSize: 14, marginLeft: 80 }}>
                        Teamed to serve you better
                    </Text>

             

                    <View style={{ flex: 1, flexDirection: 'row' }} >

                        <View style={styles.container}>
                            <Image transparent source={require('../../assets/handy.png')} style={styles.bottomImage} />
                        </View>

                        <View style={styles.container}>
                            <Image transparent source={require('../../assets/jumbo.png')} style={styles.bottomImage2} />
                        </View>

                    </View>

                    <Text style={styles.dataStyle} style={{ fontSize: 14, marginLeft: 90 }}>
                        Powered by
                    </Text>

                    <View style={styles.container}>
                        <Image transparent source={require('../../assets/comtech.png')} style={styles.bottomImage3} />
                    </View>

                </Content>
            </Container>


        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
        // padding: 10
    },

    scrollStyle: {
        backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        padding: 10,
        width: 200,
        height: 95,
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center',

    },

    bottomImage: {
        padding: 10,
        width: 120,
        height: 60,
        marginBottom: 50,
        marginLeft: -20,
        justifyContent: 'center',
        alignItems: 'center',

    },

    bottomImage2: {
        padding: 10,
        width: 160,
        height: 65,
        marginBottom: 50,
        marginLeft: -10,
        justifyContent: 'center',
        alignItems: 'center',

    },

    bottomImage3: {
        padding: 10,
        width: 250,
        height: 85,
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center',

    },

    dataStyle: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 15,
        // fontWeight: '100'
        // marginLeft: 10
    },

    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18
    },

    bodyStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

});



