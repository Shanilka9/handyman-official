import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View
} from 'react-native';

import {
    Container, Header, Content, Card, CardItem, Text, Body, Left, Right, Button, Icon, Title
} from 'native-base';

export default class Slide_About extends Component {

    constructor(props) {
        super(props);
        state = {

        }
    }

    render() {

        return (
            <Container>
                <Header style={styles.headerStyles}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>

                    <Body style={styles.bodyStyle}>
                        <Title style={styles.headerStyles}>About</Title>
                    </Body>

                    <Right>
                        <Button transparent>
                            {/* <Text>Cancel</Text> */}
                        </Button>
                    </Right>
                </Header>


                <Content padder>

                    <Card transparent>

                        <CardItem>
                            <View style={styles.container}>
                                <Image source={require('../../assets/handy.png')} style={styles.HeadericonlImage2} />
                            </View>
                        </CardItem>

                        <CardItem>
                            <Body>
                                <Text style={styles.dataStyle}>
                                    Handyman App is an exclusive {"\n"}
                                    Facility Management Service Application
                                    designed to be a 'Total Solutions Provider'{"\n"} 
                                    to all who are looking for building {"\n"}
                                    maintenance and other associated services through a single window.
                                   
                                   {"\n"} {"\n"}

                                    Handyman App is free to download and is designed to be user friendly.
                                
                                    {"\n"} {"\n"}

                                    Once downloaded to Android or IOS mobile your worry of looking for
                                    a handyman to attend to your job is taken over by Handyman App
                                    which functions as a Cell Center facilitating communication with our service
                                    providers effectively and saving your time. Handyman App currently operates
                                    in Qatar markert.

                                </Text>
                            </Body>
                        </CardItem>

                    </Card>
                </Content>
            </Container>


        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
        // padding: -20
    },

    scrollStyle: {
        backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        padding: 10,
        width: 200,
        height: 95,
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center',

    },

    dataStyle: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 15
        // marginLeft: 10
    },

    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18
    },

    bodyStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

});



