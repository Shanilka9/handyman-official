
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert,
    AsyncStorage,
    NetInfo
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import base64 from 'react-native-base64';
import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

export default class Verification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullName: '',
            username: '',
            email: '',
            password: '',
            validated: false,
            clientData: null,
            pin: '',
            clientData: {}
        }
    }


    // loginFailedAlert() {
    //     showMessage({
    //       message: "Login Failed!",
    //       description: "Pin Code Didn't Match",
    //       type: "default",
    //       backgroundColor: "#ed1212", // background color
    //       color: "white", // text color
    //     });
    //   }

    //   loginSuccessAlert() {
    //     showMessage({
    //       message: "Login Success!",
    //       description: "Pin Code Matched",
    //       type: "default",
    //       backgroundColor: "#05661a", // background color
    //       color: "white", // text color
    //     });
    //   }

    //   errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }
    

    componentWillMount() {
        this.setState({
            clientData: this.props.navigation.state.params.navAppData
        })
    }

    onClickListener = (viewId) => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BaseUrl+'api/client/verification?verifyCode=' + this.state.pin, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(this.props.navigation.state.params.navAppData.username + ":" + this.props.navigation.state.params.navAppData.password)
                    }
                })
                    .then((response) => {
                        console.log("RES:>>>" + JSON.stringify(response));
                        return response.json()
                    })
                    .then((responseJson) => {
                        console.log("Response:>>>" + JSON.stringify(responseJson.status));
                        if (responseJson.status != false) {
                            //this.loginSuccessAlert();
                            Alerts.FlashMessage(Strings.LoginSuccesswithPinCode,Strings.LoginSuccesswithPinCodeDescription,Strings.AlertIcons[1],Colors.SuccessColor,Colors.WarningTextColor)
                            // Alert.alert("Login Success", "Pin Code Match !");
                            this._storeData();
                        } else {
                            // Alert.alert("Login Failed", "Pin Code Didn't Match !");
                            Alerts.FlashMessage(Strings.LoginWithPincodeFailedAlert,Strings.LoginWithPincodeFailedDescription,Strings.AlertIcons[5],Colors.ErrorColor,Colors.WarningTextColor)
                        }
                    })
                    .catch((error) => {
                        console.log(error)
                    });
            } else {
                // Alert.alert("Connection Failed", "Please check your Network Connection !");
               // this.errorMsgForConnection();
               Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
            }
        });
    }


    verificationCodeResend(){

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected==true) {
                
              fetch(Strings.BaseUrl+'api/client/verification/resendCode', {
                method: 'PUT',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic ' + base64.encode(this.props.navigation.state.params.navAppData.username + ":" + this.props.navigation.state.params.navAppData.password)
                },
              })
                .then((response) => {
                  console.log("RESPOSE:>>>"+JSON.stringify(response)) 
                  return response.json()
                })
                .then((responseJson) => {
                
                  console.log("response - " + JSON.stringify(responseJson));
                })
      
                .catch((error) => {
                  // Alert.alert("Failed !!", "Your Request Was Not Sent")
                  console.log(error)
                });
      
            }
          });
    }

    _storeData = async () => {
        try {
            await AsyncStorage.setItem('client_data', JSON.stringify(this.state.clientData), () => {
                // Alert.alert("Login Success");
                this.props.navigation.navigate('NewDash');
            });

        } catch (error) {
            console.error(error);
        }
    }



    render() {
        return (
            <ScrollView style={styles.scrollStyle} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.container}>
                    <Image source={require('../assets/handy.png')} style={styles.HeadericonlImage2} />

                    <Text style={styles.pinText}> Enter Pin Code to Verify Your Login </Text>

                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={require('../assets/keyIcon.png')} />
                        <TextInput style={styles.inputs}
                            placeholder="PIN CODE"
                            value={this.setState.pin}
                            underlineColorAndroid='transparent'
                            onChangeText={(pin) => this.setState({ pin })}
                        />
                    </View>
                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.onClickListener()}>
                        <Text style={styles.signUpText}>Submit</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={[styles.buttonContainer, { backgroundColor: '#ffb807' }]} onPress={() => this.verificationCodeResend()}>
                        <Text style={styles.signUpText}>Resend</Text>
                    </TouchableHighlight>


                </View>
            </ ScrollView>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },

    pinText:{
        padding: 10
    },


    scrollStyle: {
        backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        width: 310,
        height: 150,
        marginBottom: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#848181',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#00b5ec',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },
    signupButton: {
        backgroundColor: "#f4c741",
    },
    signUpText: {
        color: 'white',
    }
});