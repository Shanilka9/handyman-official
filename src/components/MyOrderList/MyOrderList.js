import React, { Component } from 'react';
import {
    Container,
    FooterTab,
    Header,
    Content,
    Footer,
    ListItem,
    Text,
    Radio,
    Right,
    Left,
    Icon,
    Title,
    Body,
    Button,
    Subtitle,
    Tab,
    Tabs
} from 'native-base';

import PendingOrderList from './PendingOrderList';
import InProgressOrderList from './InProgressOrderList';
import CompletedOrderList from './CompletedOrderList';

export default class MyOrderList extends Component {


    /* radio button selection props */
    constructor(props) {
        super(props);

        this.state = {

        };
    }


    /* radio button props end */

    render() {
        const { headerStyles, footerIcon, footerStyle, footerText, nbc, headerText, nextButton, areaTopic, areaStyles, progressStyle, headerStyle, subStyle, bodyStyle } = styles
        return (
            <Container>

                {/* header strting here */}

                <Header style={headerStyle}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body style={bodyStyle}>
                        <Title style={headerStyles}>My Order List</Title>
                    </Body>

                    <Right>
                        <Button transparent>
                            {/* <Text>Cancel</Text> */}
                        </Button>
                    </Right>
                </Header>

                {/* Header close!!!!!!!!!!!!! */}




                <Tabs>
                    <Tab heading="Pending"
                        tabStyle={{ backgroundColor: '#59554a' }}
                        textStyle={{ color: '#fff' }}
                        activeTabStyle={{ backgroundColor: '#59554a' }}
                        activeTextStyle={{
                            color: '#fff',
                            fontWeight: 'normal'
                        }}
                    >
                        <PendingOrderList navigation={this.props.navigation} />

                    </Tab>
                    <Tab heading="In Progress"
                        tabStyle={{ backgroundColor: '#59554a' }}
                        textStyle={{ color: '#fff' }}
                        activeTabStyle={{ backgroundColor: '#59554a' }}
                        activeTextStyle={{ color: '#fff', fontWeight: 'normal' }}
                    >

                        {/* <Text>In Progress</Text> */}
                        <InProgressOrderList navigation={this.props.navigation} />
                    </Tab>


                    <Tab heading="Completed"
                        tabStyle={{ backgroundColor: '#59554a' }}
                        textStyle={{ color: '#fff' }}
                        activeTabStyle={{ backgroundColor: '#59554a' }}
                        activeTextStyle={{
                            color: '#fff',
                            fontWeight: 'normal'
                        }}
                    >

                        <CompletedOrderList navigation={this.props.navigation} />

                    </Tab>

                </Tabs>


            </Container>
        );
    }
}

const styles = {


    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18,

    },

    footerStyle: {
        backgroundColor: '#f0ad4e'
    },

    footerIcon: {
        width: 25,
        height: 25
    },

    footerText: {
        color: 'black'
    },

    progressStyle: {
        height: 6,
        marginTop: 0,
        backgroundColor: 'black',
        width: 60

    },

    footerStyles: {
        backgroundColor: '#f0ad4e',
        alignItems: 'center'
    },

    headerText: {
        color: 'white'
    },
    areaTopic: {
        fontWeight: 'bold'
    },

    areaStyles: {
        fontSize: 13,
        color: 'black'
    },
    headerStyle: {
        backgroundColor: '#f0ad4e'
    },
    subStyle: {
        color: 'black',
        fontSize: 12
    },
    bodyStyle: {
        flex: 1,
        flexDirection: 'column',

        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

    nextButton: {
        width: 300,
        height: 55,
        alignItems: 'center',
        backgroundColor: '#f0ad4e',

    },

    nbc: {
        color: 'black',
        fontSize: 16
    }




};