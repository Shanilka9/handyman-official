import React, { Component } from 'react';
import {
    Container,
    Content,
    TouchableOpacity,
    Text,
    Body,
    Card,
    CardItem,
} from 'native-base';
import base64 from 'react-native-base64';
import { AsyncStorage, Alert, ActivityIndicator, NetInfo, StyleSheet, View, Image } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import GridView from 'react-native-super-grid';
import Strings from '../../essentials/Strings';
import Alerts from '../../essentials/Alerts';
import Colors from '../../essentials/Colors';

export default class PendingOrderList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            clientData: {},
            appData: {},
            url: "",
            spinner: true,

            pendingJobList: [],
            tempJobList: []

        }
    }

    // noJobListAlert() {
    //     showMessage({
    //         message: "Job Lists Empty",
    //         description: "No Pending Jobs to Display",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#fcbf19", // background color
    //         color: "white", // text color
    //     });
    // }

    componentWillMount() {
        this._retrieveData();
    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_data');
            if (value !== null) {
                this.setState({
                    clientData: JSON.parse(value)
                })

                return fetch(Strings.BaseUrl+'api/app/client/job', {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password)
                    }
                })

                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            pendingJobList: responseJson.pending,
                            // inProgressJobList: responseJson.inProgress,
                            // completedJobList: responseJson.complete
                        })



                        // console.log("PendingJobList  > " + JSON.stringify(this.state.pendingJobList.length));

                        if (this.state.pendingJobList.length == 0) {
                            // Alert.alert("No Pending Jobs to Display");
                            //this.noJobListAlert();
                            Alerts.FlashMessage(Strings.noJobListAlert,Strings.noJobListAlertDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)

                            this.setState({
                                spinner: false
                            })

                        } else {
                            for (var i = 0; i < this.state.pendingJobList.length; i++) {

                                // console.log("PendingJobList  > " + JSON.stringify(this.state.pendingJobList[i]));

                                this.state.tempJobList.push(
                                    {
                                        area: this.state.pendingJobList[i].area,
                                        jobTime: this.state.pendingJobList[i].jobTime,
                                        address: this.state.pendingJobList[i].address,
                                        issue: this.state.pendingJobList[i].issue,
                                        jobDate: this.state.pendingJobList[i].jobDate,
                                        description: this.state.pendingJobList[i].description,
                                        id: this.state.pendingJobList[i].id,
                                        category: this.state.pendingJobList[i].category,
                                        jobImagePath: this.state.pendingJobList[i].jobImagePath
                                    }
                                )

                                this.setState({
                                    spinner: false
                                })

                            }

                        }




                        // console.log("response > " + JSON.stringify(responseJson));
                        console.log(this.state.clientData);
                        // console.log("response > " + JSON.stringify(this.state.pendingJobList[0]));
                    })

                    .catch((error) => {
                        console.log(error)
                    });
            }
            // console.log(this.state.clientData);
            // console.log(this.responseJson)
        } catch (error) {
            // Error retrieving data
            console.log(error);
        }
    }


    render() {

        return (

            <Container>
                <View>
                    <Spinner
                        visible={this.state.spinner}
                        textContent={'Loading...'}
                    // textStyle={styles.spinnerTextStyle}
                    />
                </View>


                <GridView
                    itemDimension={200}
                    items={this.state.tempJobList}
                    style={styles.gridView}
                    renderItem={item => (

                        <Content>

                            <Content padder>

                                <Card style={{ borderRadius: 6, borderColor: '#fcbf19', borderTopWidth: 2, borderRightWidth: 2, borderLeftWidth: 2, borderBottomWidth: 2 }}>
                                    <CardItem bordered >
                                        <Text style={styles.headerStyle}>Request ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{item.id}</Text>
                                    </CardItem>

                                    <CardItem style={{ height: 30 }}>

                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Category
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.category}</Text>
                                        </Body>

                                    </CardItem>

                                    <CardItem style={{ height: 30 }} >
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Issue
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.issue}</Text>
                                        </Body>

                                    </CardItem>

                                    <CardItem>
                                        <Body style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                            <Text style={styles.topicStyle}>Description</Text>
                                            <Text style={styles.dataStyle}>{item.description}</Text>
                                        </Body>

                                    </CardItem>

                                    <CardItem style={{ height: 30 }}>

                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Service Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.jobTime}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem style={{ height: 30 }}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Service Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.jobDate}</Text>

                                        </Body>
                                    </CardItem>

                                    <CardItem style={{ height: 30 }}>

                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Service Area&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.area}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem>
                                        <Body style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                                            <Text style={styles.topicStyle}>Address</Text>
                                            <Text style={styles.dataStyle}>{item.address}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem cardBody>
                                        <Image source={{ uri: Strings.BaseUrl + item.jobImagePath }} style={styles.stretch} />
                                    </CardItem>

                                </Card>

                            </Content>

                        </Content>




                    )}
                />

            </Container >


        );
    }

}


const styles = StyleSheet.create({

    gridView: {
        paddingTop: 8,
        flex: 1,
    },

    textBodyStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },

    textBodyStyle2: {
        flex: 1,
        flexDirection: 'row',
        // justifyContent: ''
    },

    stretch: {
        width: 200,
        height: 200,
        flex: 1,
        flexWrap: 'wrap'

    },

    topicStyle: {
        color: '#f0ad4e',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 14,
        fontWeight: 'bold'

        
    },

    dataStyle: {
        color: '#59554a',
        textAlign: 'center',
        justifyContent: 'center',
        // marginLeft: 10
        fontSize: 14,
        // fontWeight: 'bold'
    },


    headerStyle: {
        color: '#fcbf19',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 16,
        fontWeight: 'bold'

    },



    contentStyle: {
        textAlign: 'center',

    }

});