import React, { Component } from 'react';
import {
    Container,
    Content,
    TouchableOpacity,
    Text,
    Body,
    Card,
    CardItem,
} from 'native-base';
import base64 from 'react-native-base64';
import { AsyncStorage, Alert, ActivityIndicator, NetInfo, StyleSheet, View, Image } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import GridView from 'react-native-super-grid';
import Strings from '../../essentials/Strings';
import Alerts from '../../essentials/Alerts';
import Colors from '../../essentials/Colors';

export default class completedOrderList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            clientData: {},
            appData: {},
            url: "",
            spinner: true,

            completedJobList: [],
            tempJobList: []

        }
    }

    // noJobListAlert() {
    //     showMessage({
    //         message: "Job Lists Empty",
    //         description: "No Completed Jobs to Display",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#fcbf19", // background color
    //         color: "white", // text color
    //     });
    // }

    // componentDidMount() {
    //     this._retrieveData();
    // }

    componentWillMount() {
        this._retrieveData();
    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_data');
            if (value !== null) {
                this.setState({
                    clientData: JSON.parse(value)
                })

                return fetch(Strings.BaseUrl+'api/app/client/job', {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password)
                    }
                })

                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            // pendingJobList: responseJson.pending,
                            // inProgressJobList: responseJson.inProgress,
                            completedJobList: responseJson.complete
                        })

                        // console.log("PendingJobList  > " + JSON.stringify(responseJson));


                        if (this.state.completedJobList.length == 0) {
                            // Alert.alert("No Completed Jobs to Display");
                            //this.noJobListAlert();
                            Alerts.FlashMessage(Strings.noJobListAlert,Strings.noJobListAlertDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)

                            this.setState({
                                spinner: false
                            })

                        } else {

                            for (var i = 0; i < this.state.completedJobList.length; i++) {

                                console.log("In Progress JobList  > " + JSON.stringify(this.state.completedJobList[i].jobDate));

                                this.state.tempJobList.push(
                                    {
                                        area: this.state.completedJobList[i].area,
                                        jobTime: this.state.completedJobList[i].jobTime,
                                        address: this.state.completedJobList[i].address,
                                        issue: this.state.completedJobList[i].issue,
                                        assignDate: this.state.completedJobList[i].assignDate,
                                        jobDate: this.state.completedJobList[i].jobDate,
                                        description: this.state.completedJobList[i].description,
                                        id: this.state.completedJobList[i].id,
                                        category: this.state.completedJobList[i].category,
                                        jobImagePath: this.state.completedJobList[i].jobImagePath,

                                        providerId: this.state.completedJobList[i].serviceProvider.id,
                                        providerName: this.state.completedJobList[i].serviceProvider.name,
                                        providerArea: this.state.completedJobList[i].serviceProvider.area,
                                        providerAddress: this.state.completedJobList[i].serviceProvider.address,
                                        providerMobile: this.state.completedJobList[i].serviceProvider.mobile,
                                        providerEmail: this.state.completedJobList[i].serviceProvider.email,
                                        providerContactPersonName: this.state.completedJobList[i].serviceProvider.contactPersonName,
                                        providerContactPersonNumber: this.state.completedJobList[i].serviceProvider.contactPersonNumber,

                                    }
                                )

                                this.setState({
                                    spinner: false
                                })

                            }
                        }

                        // console.log("response > " + JSON.stringify(responseJson));
                        console.log(this.state.clientData);
                        // console.log("response > " + JSON.stringify(this.state.pendingJobList[0]));
                    })

                    .catch((error) => {
                        console.log(error)
                    });
            }
            // console.log(this.state.clientData);
            // console.log(this.responseJson)
        } catch (error) {
            // Error retrieving data
            console.log(error);
        }
    }


    render() {

        return (

            <Container>
                <View>
                    <Spinner
                        visible={this.state.spinner}
                        textContent={'Loading...'}
                    />
                </View>


                <GridView
                    itemDimension={200}
                    items={this.state.tempJobList}
                    style={styles.gridView}
                    renderItem={item => (

                        <Content>

                            <Content padder>
                                <Card style={{ borderRadius: 6, borderColor: '#fcbf19', borderTopWidth: 2, borderRightWidth: 2, borderLeftWidth: 2, borderBottomWidth: 2 }}>

                                    <CardItem bordered >
                                        <Text style={styles.headerStyle}>Request ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{item.id}</Text>
                                    </CardItem>

                                    <CardItem style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Category
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.category}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Issue
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.issue}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem>
                                        <Body style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                            <Text style={styles.topicStyle}>Description</Text>
                                            <Text style={styles.dataStyle}>{item.description}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem  style={styles.cardStyle}>

                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Service Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.jobTime}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Service Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.jobDate}</Text>

                                        </Body>
                                    </CardItem>

                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Assigned Date&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.assignDate}</Text>

                                        </Body>
                                    </CardItem>

                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Service Area&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.area}</Text>

                                        </Body>
                                    </CardItem>

                                    <CardItem>
                                        <Body style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                                            <Text style={styles.topicStyle}>Address</Text>
                                            <Text style={styles.dataStyle}>{item.address}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem cardBody>
                                        <Image source={{ uri: Strings.BaseUrl + item.jobImagePath }} style={styles.stretch} />
                                    </CardItem>

                                    <CardItem bordered >
                                        <Text style={styles.headerStyle}>Service Provider Details</Text>
                                    </CardItem>

                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Provider Name&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.providerName}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Area&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.providerArea}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem>
                                        <Body style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
                                            <Text style={styles.topicStyle}>Address</Text>
                                            <Text style={styles.dataStyle}>{item.providerAddress}</Text>
                                        </Body>
                                    </CardItem>
                                    
                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Mobile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.providerMobile}</Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.providerEmail}</Text>
                                        </Body>
                                    </CardItem>


                                    <CardItem  style={styles.cardStyle}>
                                        <Body style={styles.textBodyStyle2}>
                                            <Text style={styles.topicStyle}>Contact&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Text style={styles.dataStyle}>{item.providerContactPersonName} ({item.providerContactPersonNumber}) </Text>
                                        </Body>
                                    </CardItem>

                                    <CardItem style={{ height: 10 }}>
                                        
                                    </CardItem>

                                </Card>

                            </Content>

                        </Content>




                    )}
                />

            </Container >


        );
    }

}


const styles = StyleSheet.create({

    gridView: {
        paddingTop: 8,
        flex: 1,
    },

    headerStyle: {
        color: '#fcbf19',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 16,
        fontWeight: 'bold'

    },
    
    cardStyle:{
        height: 30
    },

    stretch: {
        // width: 'auto',
        width: 200,
        // height: 'auto',
        height: 200,
        flex: 1,
        flexWrap: 'wrap'

    },

    textBodyStyle2: {
        flex: 1,
        flexDirection: 'row',
        // justifyContent: ''
    },

    topicStyle: {
        color: '#f0ad4e',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 14,
        fontWeight: 'bold'

        
    },

    dataStyle: {
        color: '#59554a',
        textAlign: 'center',
        justifyContent: 'center',
        // marginLeft: 10
        fontSize: 14,
        // fontWeight: 'bold'
    },


    contentStyle: {
        textAlign: 'center',

    }

});