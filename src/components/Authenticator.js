
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    AsyncStorage,
    NetInfo,
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation'
import Alerts from '../essentials/Alerts';
import Strings from '../essentials/Strings';
import Colors from '../essentials/Colors';

export default class Authenticator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullName: '',
            username: '',
            email: '',
            password: '',
            validated: false,
            clientData: null
        }
    }

    componentDidMount() {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                this._retrieveData();
            } else {
                Alerts.FlashMessage(Strings.ConnectionErrorMessage, Strings.ConnectionErrorDescription, Strings.AlertIcons[2],Colors.ErrorColor,Colors.AlertTextColor);
                // Alerts.errorMsgForConnection();
                // Alert.alert("Error", "No Internet Connection")
            }
        });
    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('login');
            // console.log("Value:" + value);
            if (value === 'true') {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'NewDash' })],
                });
                this.props.navigation.dispatch(resetAction);
            } else {
                // console.log("Run True");
                // this.props.navigation.navigate('SignIn');
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'SignIn' })],
                });
                this.props.navigation.dispatch(resetAction);

            }
        } catch (error) {
            // Error retrieving data
            console.log(error);
        }
    }




    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../assets/handy.png')} style={styles.HeadericonlImage2} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
        padding: 30
    },

    HeadericonlImage2: {
        // padding: 3,
        width: 300,
        height: 150,
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center',
        // marginLeft: -10
    },


});