import React, { Component } from 'react';
import { Container, Header, View, Content, Footer, ListItem, Text, Radio, Right, Left, Icon, Title, Body, Button, Subtitle } from 'native-base';
import { AsyncStorage, StyleSheet, ActivityIndicator, DeviceEventEmitter, BackHandler, NetInfo, Alert } from 'react-native';
import base64 from 'react-native-base64';
import RadioForm from 'react-native-radio-form';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

export default class ServiceArea extends Component {


    _onSelect = (item) => {
        this.state.appData.areaId = item.value;
        this.state.appData.areaName = item.label;
        console.log(item);
    };

    constructor(props) {
        super(props);
        this.state = {
            dataSourse: null,
            clientData: null,
            radioDataSourse: [],
            tempRadioDataSourse: [],
            username: "",
            password: "",
            appData: {},
            isLoading: true
        };
    }

    // static defaultProps = {
    //     mockdata: [],
    //   };

    componentWillMount() {
        // const { mockdata } = this.props;
        this.setState({
            appData: this.props.navigation.state.params.navAppData
        })
        this._retrieveData();

        // BackHandler.addEventListener('hardwareBackPress', () => { //(optional) you can use it if you need it
        //     //do not use this method if you are using navigation."preventBackClick: false" is already doing the same thing.
        //     LocationServicesDialogBox.forceCloseDialog();
        //     this.props.navigation.goBack();

        // });
        DeviceEventEmitter.addListener('locationProviderStatusChange', function (status) { // only trigger when "providerListener" is enabled
            console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
        });
        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2 style='color: #0af13e'>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
            ok: "YES",
            cancel: "NO",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: false, // true => To prevent the location services window from closing when it is clicked outside
            preventBackClick: false, // true => To prevent the location services popup from closing when it is clicked back button
            providerListener: false // true ==> Trigger locationProviderStatusChange listener when the location state changes
        }).then(function (success) {
            console.log(success); // success => {alreadyEnabled: false, enabled: true, status: "enabled"}
        }).catch((error) => {
            console.log(error.message); // error.message => "disabled"
        });
    }
    componentWillUnmount() {
        // used only when "providerListener" is enabled
        LocationServicesDialogBox.stopListener(); // Stop the "locationProviderStatusChange" listener
    }

    onClickListener() {


        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                this.props.navigation.navigate('Map', {
                    navAppData: this.state.appData
                });


                // this._retrieveData();
            } else {
                //this.errorMsgForConnection();
                // Alert.alert("Error", "No Internet Connection")
                Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
            }
        });


        // this.props.navigation.navigate('Map', {
        //     navAppData: this.state.appData
        // });
    }

    // errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_data');
            if (value !== null) {
                this.setState({
                    clientData: JSON.parse(value)
                })

                return fetch(Strings.BaseUrl+'api/app/initializeDetail', {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password)
                    }
                })

                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            radioDataSourse: responseJson.areas
                        })
                        // console.log("areas - " + JSON.stringify(this.state.responseJson.areas.name));

                        // console.log("areas - " + JSON.stringify(this.state.radioDataSourse));

                        for (var i = 0; i < this.state.radioDataSourse.length; i++) {
                            console.log("areas  " + this.state.radioDataSourse[i]);
                            this.state.tempRadioDataSourse.push({
                                label: this.state.radioDataSourse[i].name,
                                value: this.state.radioDataSourse[i].id
                            })
                        }

                        this.state.appData.areaId = this.state.tempRadioDataSourse[0].value;
                        this.state.appData.areaName = this.state.tempRadioDataSourse[0].label;
                        this.setState({
                            isLoading: false
                        })

                    })

                    .catch((error) => {
                        console.log(error)
                    });
            }

        } catch (error) {
            // Error retrieving data
            console.log(error);


        }
    }

    /* radio button props end */

    render() {
        if (this.state.isLoading) {
            // this._retrieveData();
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size='large' color='#f0ad4e' />
                </View>
            )
        } else {
            return (
                <Container>

                    {/* header strting here */}

                    <Header style={styles.headerStyle}>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.goBack()}>
                                <Icon name='arrow-back' />
                            </Button>
                        </Left>
                        <Body style={styles.bodyStyle}>
                            <Title style={styles.headerStyles}>Select Area</Title>
                        </Body>

                        <Right>
                            <Button transparent>
                                <Text>Cancel</Text>
                            </Button>
                        </Right>
                    </Header>

                    {/* Header close!!!!!!!!!!!!! */}


                    <Content padder>
                        <View style={styles.container}>
                            <View style={{ marginHorizontal: 10 }} >
                                <RadioForm
                                    style={{ width: 350 - 30 }}
                                    dataSource={this.state.tempRadioDataSourse}
                                    itemShowKey="label"
                                    itemRealKey="value"
                                    circleSize={18}
                                    initial={0}
                                    formHorizontal={false}
                                    labelHorizontal={true}
                                    onPress={(item) => this._onSelect(item)}
                                />

                            </View>
                        </View>
                    </Content>




                    {/* footer start */}

                    <Footer style={styles.footerStyles}>

                        {/* buttton for navigete to next page */}
                        <Button transparent style={styles.nextButton} full onPress={() => this.onClickListener()}>
                            <Text style={styles.nbc}>NEXT</Text>

                        </Button>

                    </Footer>
                    {/* footer end */}
                </Container>
            );
        }

    }
}

const styles = StyleSheet.create({

    container: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },

    loadingContainer: {
        flex: 1,
        justifyContent: 'center'
    },


    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18,

    },

    progressStyle: {
        height: 6,
        marginTop: 0,
        backgroundColor: 'black',
        width: 60

    },

    footerStyles: {
        backgroundColor: '#f0ad4e',
        alignItems: 'center'
    },

    headerText: {
        color: 'white'
    },
    areaTopic: {
        fontWeight: 'bold'
    },

    areaStyles: {
        fontSize: 13,
        color: 'black'
    },
    headerStyle: {
        backgroundColor: '#f0ad4e'
    },
    subStyle: {
        color: 'black',
        fontSize: 12
    },
    bodyStyle: {
        flex: 1,
        flexDirection: 'column',

        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

    nextButton: {
        width: 300,
        height: 55,
        alignItems: 'center',
        backgroundColor: '#f0ad4e',

    },

    nbc: {
        color: 'black',
        fontSize: 16
    }




});