import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    AsyncStorage,
    BackHandler,
    ActivityIndicator
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Container, Header, Footer, FooterTab, Text, Toast, Right, Body, Left, Title, Icon, Button, CardItem, Card, Drawer } from 'native-base';
import GridView from 'react-native-super-grid';
import base64 from 'react-native-base64';
import SideBar from './SideBar';


import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

export default class NewDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSourse: [],
            username: "",
            password: "",
            clientData: null,
            spinner: false,
            place: "DASH",
            appData: {
                categoryId: 0,
                categoryName: "",
                categoryIssue: "",
                description: "",
                date: "",
                time: "",
                areaId: 0,
                areaName: "",
                address: "",
                image: null,
            },
            isLoading: false
        }
    }

    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        console.log("Press");
        this.drawer._root.open()
    };

    componentDidMount() {
        this._retrieveData();
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillMount() {
        this.setState({
            spinner: true
        })
        this._storeData();
    }

    refresh(){
        this.setState({
            place:'DASH'
        });
    }

    handleBackPress = () => {
        if (this.state.place == 'DASH') {
            BackHandler.exitApp();
            console.log("Exit")
        } else {
            this.props.navigation.goBack()
        }
        console.log("BACKPRESS")
        return true;
    }

    componentWillUpdate() {
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    onClickGridListener = (viewId, viewName) => {
        this.setState({
            spinner: false,
            place:'DASH-NONE'
        })
        // BackHandler.removeEventListener('hardwareBackPress',null);
        this.state.appData.categoryId = viewId;
        this.state.appData.categoryName = viewName
        this.props.navigation.navigate('Issue', {
            navAppData: this.state.appData
        });
        console.log(this.state.appData.categoryName);
    }

    logOut() {
        this.out();
    }


    out = async () => {
        try {
            await AsyncStorage.removeItem('login');
            // Alert.alert("Success","You Have SignOut Successfully!")

            Alerts.FlashMessage(Strings.LogoutMesseage, Strings.LogoutDescription,Strings.AlertIcons[1],Colors.SuccessColor,Colors.AlertTextColor);

            this.props.navigation.navigate('SignIn')
        } catch (error) {
            console.error(error);
        }

    }


    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_data');
            if (value !== null) {
                this.setState({
                    clientData: JSON.parse(value)
                })

                return fetch(Strings.BaseUrl+'api/app/initializeDetail', {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password)
                    }
                })

                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            dataSourse: responseJson.categorys,
                            spinner: false
                        })
                    })

                    .catch((error) => {


                        // this.setState({
                        //     spinner: false
                        // })
                        this.setState({
                            spinner: false
                        })
                        console.log(error)
                    });
            }
            // console.log(this.state.clientData);
            // console.log(this.responseJson)
        } catch (error) {
            // Error retrieving data
            console.log(error);


        }

    }

    _storeData = async () => {
        try {
            await AsyncStorage.setItem('login', 'true', () => {
                console.log("Saved Login....")
            });
        } catch (error) {
            console.error(error);
        }
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View>
                    <ActivityIndicator />
                </View>
            )
        } else {
            return (

                <Drawer
                    ref={(ref) => { this.drawer = ref; }}
                    content={<SideBar navigation={this.props.navigation} />}
                    onClose={() => this.closeDrawer()}
                    tweenHandler={(ratio) => ({
                        main: { opacity: (1 - ratio) / 1 }
                    })}>
                    <Container>
                        <Header style={styles.headerStyle} >

                        {/* <Button transparent onPress={() => this.openDrawer()}></Button> */}

                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon name='md-menu' style={{ fontSize: 25, color: 'black', }}   >
                                </Icon>
                            </Button>
                            <Left></Left>
                            <Image source={require('../assets/handy.png')} style={styles.HeaderIconlImage2} />
                            <Right>
                                <Button transparent>
                                    <Icon name='md-log-out' style={{ fontSize: 25, color: 'black', }} onPress={() => this.logOut()} >
                                    </Icon>
                                </Button>
                            </Right>
                        </Header>

                        <Spinner
                            visible={this.state.spinner}
                            textContent={'Loading...'}

                        />
                        <GridView
                            itemDimension={90}
                            items={this.state.dataSourse}
                            style={styles.gridView}
                            renderItem={item => (
                                <View style={[styles.itemContainer, { backgroundColor: '#ffffff' }]} >
                                    <Button transparent onPress={() => this.onClickGridListener(item.id, item.name)} style={{ width: 70, height: 70 }} >
                                        <Image
                                            style={{ width: 50, height: 50, marginLeft: 20 }}
                                            source={{ uri: Strings.BaseUrl + item.imagePath }}

                                        />
                                    </Button>
                                    <Text style={styles.itemName}>{item.name}</Text>
                                </View>
                            )}
                        />

                        <Footer >
                            <FooterTab style={styles.footerStyle}>
                                <Button vertical>
                                    {/* <Icon name="navigate" style={{ color: 'yellow' }} /> */}
                                    <Image source={require('../assets/order-list.png')} style={styles.footerIcon} />
                                    <Text style={styles.footerText}> Order </Text>
                                </Button>
                                <Button vertical onPress={() => this.props.navigation.navigate('MyOrder')}>
                                    <Image source={require('../assets/my-order.png')} style={styles.footerIcon} />
                                    <Text style={styles.footerText}>My Order</Text>
                                </Button>
                                {/* <Button vertical>
                                    <Image source={require('../assets/more.png')} style={styles.footerIcon} />
                                    <Text style={styles.footerText}>More</Text>
                                </Button> */}
                                <Button vertical onPress={() => this.props.navigation.navigate('Provider')}>
                                    <Image source={require('../assets/more.png')} style={styles.footerIcon} />
                                    <Text style={styles.footerText}>Provider Request</Text>
                                </Button>

                            </FooterTab>
                        </Footer>
                        
                    </Container>
                </Drawer>
            );
        }
    }
}

const styles = StyleSheet.create({

    cardContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },

    card: {
        flex: 1,
        width: 150,
        height: 150,
        marginTop: 10,
        marginBottom: 10,
        marginRight: 10,
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        borderRadius: 20,
        // backgroundColor: 'transparent',
    },
    cardText: {
        marginTop: 10,
        // backgroundColor: 'black',
        // color:'#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'

    },
    headerStyle: {
        backgroundColor: '#ffffff',
        height: 75,
    },

    footerIcon: {
        width: 25,
        height: 25
    },

    footerStyle: {
        backgroundColor: '#f0ad4e'
    },

    footerText: {
        color: 'black',
        fontSize: 10,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign:'center'
        
    },


    bodyStyle: {
        backgroundColor: 'transparent',
    },
    HeadericonlImage2: {
        width: 160,
        height: 75,
        // marginLeft:120
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconImages: {
        width: 50,
        height: 50,
        // marginLeft:120
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
    fab: {
        width: 50,
        height: 50,
        borderRadius: 30,
        backgroundColor: '#0a7334',
        position: 'absolute',
        top: 10,
        left: 10,
    },
    inputTextContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        // backgroundColor: 'transparent',
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#ededed'
    },
    discountInputContainer: {
        borderRadius: 30,
        // borderBottomWidth: 1,
        // borderTopWidth: 1,
        // borderRightWidth: 1,
        // borderLeftWidth: 1,
        width: null,
        height: 40,
        // marginTop: 10,
        flexDirection: 'row',
        backgroundColor: '#ffff',
        alignItems: 'center',
        color: 'black',
        // backgroundColor: 'transparent',
    },
    discountInputs: {
        height: 40,
        textAlign: 'center',
        flex: 1,
        fontSize: 15,
        color: 'black',
        // backgroundColor: 'transparent',
    },
    gridView: {
        paddingTop: 25,
        marginBottom: 25,
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    itemName: {
        fontSize: 16,
        color: '#000000',
        fontWeight: '200',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
    },


    HeaderIconlImage: {
        width: 30,
        height: 30
    },
    HeaderIconlImage2: {
        width: 110,
        height: 50,
        marginTop: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconlImage: {

        marginLeft: 34,
        width: 45,
        height: 45,

        alignItems: 'center',
        justifyContent: 'center'


    },

});

