import React, { Component } from 'react';
import { Container, Header, Content, Footer, Right, Text, Form, Textarea, Left, Button, Icon, Body, Title, Subtitle, View, Card, CardItem, } from 'native-base';
import base64 from 'react-native-base64';
import { AsyncStorage, Alert, ActivityIndicator, NetInfo, Image } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import Alerts from '../essentials/Alerts';
import Strings from '../essentials/Strings';
import Colors from '../essentials/Colors';


export default class OrderSummery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            clientData: {},
            appData: {},
            url: "",
            isLoading: false,
            spinner: false,
            image: null
        }
    }


    renderAsset(image) {
        if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
            return this.renderVideo(image);
        }

        return this.renderImage(image);
    }

    renderImage(image) {
        return <Image style={{
            flex: 1, width: 328, height: 210, resizeMode: 'contain', alignItems: 'center', flexDirection: 'row',
            justifyContent: 'center',
        }} source={image} />
    }

    renderVideo(video) {
        return (<View style={{ height: 300, width: 300 }}>
            <Video source={{ uri: video.uri, type: video.mime }}
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0
                }}
                rate={1}
                paused={false}
                volume={1}
                muted={false}
                resizeMode={'cover'}
                onError={e => console.log(e)}
                onLoad={load => console.log(load)}
                repeat={true} />
        </View>);
    }


    componentWillMount() {
        this._retrieveData();
        console.log("DATE:" + this.props.navigation.state.params.navAppData.date);

        this.setState({
            image: this.props.navigation.state.params.navAppData.image,

        })
    }

    imageUpload() {
        let body = new FormData();
        body.append('file', { uri: this.props.navigation.state.params.navAppData.image.uri, name: 'photo.jpg', filename: 'imageName.jpg', type: 'image/jpg' });
        body.append('Content-Type', 'image/jpg');
        console.log("data:" + JSON.stringify(body));
        console.log("Cinet Data" + this.state.clientData.username + ":" + this.state.clientData.password)
        fetch(Strings.BaseUrl+'api/file', {
            method: 'POST',
            headers: {
                'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password),
                'Content-Type': 'multipart/form-data',
            },
            body: body
        }).then((resp) => {
            this.setState({
                url: JSON.parse(resp._bodyText)
            })
            console.log("resp: "+ JSON.stringify(resp))
            console.log("resp: "+ JSON.stringify(resp._bodyText))
            console.log("ImageUrl: "+ JSON.stringify(this.state.url))
            // this.uplaodData();
        }).catch((err) => {
            console.log("er:::" + err)
        })

    }

    uplaodData() {
        var data = JSON.stringify({
            "categoryId": this.props.navigation.state.params.navAppData.categoryId.toString(),
            "issue": this.props.navigation.state.params.navAppData.categoryIssue.toString(),
            "jobTime": this.props.navigation.state.params.navAppData.time.toString(),
            "jobImagePath": this.state.url.url,
            "jobDate": this.props.navigation.state.params.navAppData.date.toString(),
            "areaId": this.props.navigation.state.params.navAppData.areaId.toString(),
            "description": this.props.navigation.state.params.navAppData.description.toString(),
            "address": this.props.navigation.state.params.navAppData.address.toString()
        });

        fetch(Strings.BaseUrl+'api/app/client/job', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password)
            },
            body: data
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    spinner: false
                })
                // Alert.alert("Success", "Your Request Was Sent Successfully!")

                Alerts.FlashMessage(Strings.RequestSuccessAlert, Strings.RequestSuccessDescription, Strings.AlertIcons[1],Colors.SuccessColor,Colors.AlertTextColor);

                
                this.props.navigation.navigate('NewDash');
                console.log("response - " + JSON.stringify(responseJson));
            })

            .catch((error) => {
                // Alert.alert("Failed !!", "Your Request Was Not Sent")
                Alerts.FlashMessage(Strings.RequestFailedAlert, Strings.RequestFailedDescription, Strings.AlertIcons[2],Colors.ErrorColor,Colors.AlertTextColor);
                console.log(error)
            });



    }

    onClickListener() {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                this.setState({
                    spinner: true
                })
                this.imageUpload();


            } else {
                Alerts.FlashMessage(Strings.ConnectionErrorMessage, Strings.ConnectionErrorDescription, Strings.AlertIcons[2],Colors.ErrorColor,Colors.AlertTextColor);

                // Alert.alert("Error", "No Internet Connection")
            }
        });

    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_data');
            if (value !== null) {
                this.setState({
                    clientData: JSON.parse(value)
                })
                console.log("cData", this.state.clientData);
            } else {
                console.log("cData", this.state.clientData);
            }
        } catch (error) {
            // Error retrieving data
            console.log(error);
        }

    }

    render() {
        const { headerStyles, nextButton, topicStyle, dataStyle, nbc, footerStyles, bodyStyle, headerStyle, areaTopic, subTopicStyles, textAreaStyle, subStyle, progressStyle } = styles
        if (this.state.isLoading) {
            return (
                <View>
                    <ActivityIndicator />
                </View>
            )
        } else {
            return (
                <Container>

                    <Header style={headerStyle}>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.goBack()}>
                                <Icon name='arrow-back' />
                            </Button>
                        </Left>
                        <Body style={bodyStyle}>
                            <Title style={headerStyles}>Summary</Title>

                        </Body>
                        <Right>
                            <Button transparent>
                                <Text>Cancel</Text>
                            </Button>
                        </Right>
                    </Header>

                    <Spinner
                        visible={this.state.spinner}
                        textContent={'Sending Request'}

                    />


                    <Content padder>
                        <Card style={{ borderRadius: 6, borderColor: '#fcbf19', borderTopWidth: 2, borderRightWidth: 2, borderLeftWidth: 2, borderBottomWidth:2 }}>
                            <CardItem  style={{height: 35}}>
                                <Body style={styles.textBodyStyle}>
                                    <Text style={topicStyle}>Service Category&nbsp;&nbsp;</Text>
                                    <Text style={dataStyle}>{this.props.navigation.state.params.navAppData.categoryName}</Text>

                                </Body>
                            </CardItem>


                            <CardItem  style={{height: 35}}>
                                <Body style={styles.textBodyStyle}>
                                    <Text style={topicStyle}>Issue&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                    <Text style={styles.dataStyle}> {this.props.navigation.state.params.navAppData.categoryIssue}</Text>
                                </Body>
                            </CardItem>

                            <CardItem >
                                <Body style={{flex:1, flexDirection: 'column'}}>
                                    <Text style={topicStyle}>Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                    <Text>&nbsp;</Text>                                    
                                    <Text style={styles.dataStyle}> {this.props.navigation.state.params.navAppData.description}</Text>
                                    <Text>&nbsp;</Text>

                                </Body>
                            </CardItem>

                            <CardItem  style={{height: 35}}>
                                <Body style={styles.textBodyStyle}>
                                    <Text style={topicStyle}>Service Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                    <Text style={styles.dataStyle}> {this.props.navigation.state.params.navAppData.time}</Text>
                                </Body>
                            </CardItem>

                            <CardItem  style={{height: 35}}>
                                <Body style={styles.textBodyStyle}>
                                    <Text style={topicStyle}>Service Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                    <Text style={styles.dataStyle}> {this.props.navigation.state.params.navAppData.date}</Text>
                                </Body>
                            </CardItem>

                            <CardItem  style={{height: 35}}>
                                <Body style={styles.textBodyStyle}>
                                    <Text style={topicStyle}>Service Area&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                    <Text style={styles.dataStyle}> {this.props.navigation.state.params.navAppData.areaName}</Text>
                                </Body>
                            </CardItem>

                            <CardItem >
                                <Body style={{flex:1, flexDirection: 'column'}}>
                                    <Text style={topicStyle}>Address</Text>
                                    <Text>&nbsp;</Text>
                                    <Text style={styles.dataStyle}>{this.props.navigation.state.params.navAppData.address}</Text>
                                    <Text>&nbsp;</Text>
                                </Body>
                            </CardItem>

                            <CardItem >
                                <Body style={{flex:1, flexDirection: 'row'}} >
                                    <Text style={topicStyle}>Image  </Text>
                                    {this.state.image ? this.renderAsset(this.state.image) : null}
                                </Body>
                            </CardItem>


                        </Card>
                    </Content>

                    <Footer style={footerStyles}>
                        <Button transparent style={nextButton} full onPress={() => this.onClickListener()}>
                            <Text style={nbc}>CONFIRM AND SEND</Text>
                        </Button>

                    </Footer>
                </Container>
            );
        }
    }
}

const styles = {

    topicStyle: {
        color: '#f0ad4e',
        textAlign: 'center',
        justifyContent: 'center',
        // fontSize: '14',
        fontWeight: 'bold',
        
    

    },

    dataStyle: {
        textAlign: 'center',
        justifyContent: 'center',
        // marginLeft: 10
        // fontSize: '14',


    },

    textBodyStyle:{
        // fontSize: 14,
        flex:1, 
        flexDirection: 'row',
        // justifyContent: ''
    },


    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18
    },

    footerStyles: {
        backgroundColor: '#f0ad4e',
        alignItems: 'center'
    },

    headerText: {
        color: 'white'


    },


    headerStyle: {

        backgroundColor: '#f0ad4e'
    },

    areaTopic: {
        marginTop: 15,
        fontWeight: 'bold',
        fontSize: 18

    },

    subTopicStyles: {
        fontSize: 14,
        color: 'green',

    },

    textAreaStyle: {
        backgroundColor: '#e0e3e5'

    },

    headerText: {
        color: 'white'
    },

    subStyle: {
        color: 'black',
        fontSize: 12
    },

    bodyStyle: {

        flex: 1,
        flexDirection: 'column',

        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

    nextButton: {
        width: 300,
        height: 55,
        alignItems: 'center',
        backgroundColor: '#f0ad4e',

    },

    nbc: {
        color: 'black',
        fontSize: 16
    },


    progressStyle: {
        height: 6,
        marginTop: 0,
        backgroundColor: 'black',
        width: 360

    }




};