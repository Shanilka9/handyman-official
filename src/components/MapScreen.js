import React, { Component } from 'react';
import { Container, Header, Content, Footer, Right, Text, Form, Textarea, Left, Button, Icon, Body, Title, Subtitle, View } from 'native-base';
import { StyleSheet, PermissionsAndroid, Alert, NetInfo } from 'react-native';
import MapView, { PROVIDER_GOOGLE, LATITUDE, LONGITUDE, LATITUDE_DELTA, LONGITUDE_DELTA, PROVIDER_DEFAULT } from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import Alerts from '../essentials/Alerts';

export default class MapScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            marker: {
                latlng: {
                    latitude: null,
                    longitude: null,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }
            },
            locationAddress: '',
            appData: {}
        }

        // requestCameraPermission();
    }

    
    componentWillMount() {
        Geocoder.init('AIzaSyBSG46CNezqjoRmz0KHz6YfBtQMLFi3rC8');
        this.setState({
            appData: this.props.navigation.state.params.navAppData
        })
    }

    onClickListener() {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                // console.log(this.state.lastLat + ":" + this.state.lastLong);
                Geocoder.from(this.state.lastLat, this.state.lastLong).then(json => {
                    var addressComponent = json.results[0].formatted_address;
                    // console.log("Address:"+JSON.stringify(json));
                    this.setState({
                        location: addressComponent
                    })
                    this.state.appData.address = addressComponent
                    this.props.navigation.navigate('Summery', {
                        navAppData: this.state.appData
                    });
                    // console.log("Address1:" + JSON.stringify(addressComponent))
                    console.log("Address1:" + this.state.location)
                })
                    .catch(error => {
                        Alert.alert("Loading", "Fetching Location Please wait...");
                        // console.warn(error)
                    });
                // this._retrieveData();
            } else {
                Alerts.FlashMessage(Strings.ConnectionErrorMessage, Strings.ConnectionErrorDescription, Strings.AlertIcons[2],Colors.ErrorColor,Colors.AlertTextColor);
                // Alert.alert("Error", "No Internet Connection")
            }
        });
    }

    state = {
        mapRegion: null,
        lastLat: null,
        lastLong: null,
    }

    async componentDidMount() {
        await request_location_runtime_permission()
        this.watchID = navigator.geolocation.watchPosition((position) => {
            // Create the object to update this.state.mapRegion through the onRegionChange function
            let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
            }
            this.onRegionChange(region, region.latitude, region.longitude);
        }, (error) => console.log(error));
    }

    onMapPress(e) {
        alert("coordinates:" + JSON.stringify(e.nativeEvent.coordinate))
        this.setState({
            marker: [{ coordinate: e.nativeEvent.coordinate }]
        })
    }

    onRegionChange(region, lastLat, lastLong) {
        this.setState({
            mapRegion: region,
            // If there are no new values set the current ones
            lastLat: lastLat || this.state.lastLat,
            lastLong: lastLong || this.state.lastLong
        });
    }


    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    markers(location, id) {
        let color = !id ?
            "gray" : "green";
        return (
            <MapView.Marker coordinate={{
                latitude: location.latitude,
                longitude: location.longitude
            }} pinColor={color} />
        )
    }
    // 
    render() {
        let props = this.props;
        const { headerStyles, mapStyle, nextButton, nbc, footerStyles, bodyStyle, headerStyle, areaTopic, subTopicStyles, textAreaStyle, subStyle, progressStyle } = styles

        return (
            <Container>

                <MapView
                    style={styless.map}
                    region={this.state.mapRegion}
                    showsUserLocation={true}
                    followUserLocation={true}
                // onRegionChange={this.onRegionChange.bind(this)}
                >
                    <MapView.Marker
                        coordinate={{
                            latitude: (this.state.lastLat) || -36.82339,
                            longitude: (this.state.lastLong) || -73.03569,
                        }}
                    >

                    </MapView.Marker>
                </MapView>



                <Header style={headerStyle}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back' />

                        </Button>
                    </Left>
                    <Body style={bodyStyle}>
                        <Title style={headerStyles}>Select Location</Title>

                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.onClickListener()}>
                            <Text>
                                Next
                          </Text>

                        </Button>
                    </Right>
                </Header>

            </Container>
        );
    }
}


const styless = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    }
});

const styles = {
    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18
    },

    footerStyles: {
        backgroundColor: '#f0ad4e',
        alignItems: 'center'
    },

    headerText: {
        color: 'white'


    },

    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 16
    },

    areaTopic: {
        fontWeight: 'bold'


    },

    headerStyle: {
        backgroundColor: '#f0ad4e'
    },

    subTopicStyles: {
        fontSize: 14,
        color: 'green'
    },

    textAreaStyle: {
        backgroundColor: '#e0e3e5'

    },

    headerText: {
        color: 'white'
    },

    subStyle: {
        color: 'black',
        fontSize: 12
    },

    bodyStyle: {

        flex: 1,
        flexDirection: 'column',

        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },


    progressStyle: {
        height: 4,
        marginTop: 0,
        backgroundColor: 'black',
        width: 240

    },
    /*nbc and nextButon are used for the button on the footer */
    nextButton: {
        width: 480,
        height: 55,
        alignItems: 'center',
        backgroundColor: '#f0ad4e',

    },

    nbc: {
        color: 'black',
        fontSize: 16
    },




    mapContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,

    },


    mapStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    }



};

export async function request_location_runtime_permission() {

    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            // {
            //   'title': 'ReactNativeCode Location Permission',
            //   'message': 'ReactNativeCode App needs access to your location '
            // }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {

            // Alert.alert("Location Permission Granted.");
        }
        else {

            Alert.alert("Location Permission Not Granted");

        }
    } catch (err) {
        //   console.warn(err)
    }
}