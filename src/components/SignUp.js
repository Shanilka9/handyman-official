import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  NetInfo,
  ActivityIndicator,
  Picker
} from 'react-native';
import { Content, Item, Input, Label } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

export default class SignUp extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fullName: '',
      address: '',
      email: '',
      phone: '',
      username: '',
      password: '',
      validated: false,
      pickerItems: [],
      pickerData: [],
      isLoading: false,
      clientData: {}
    }
  }

  // FullnameEmptyAlert() {
  //   showMessage({
  //     message: "Fullname Field Empty",
  //     description: "Please Enter FullName",
  //     type: "danger",
  //     icon: "danger",
  //     backgroundColor: "#f4c741", // background color
  //     color: "black", // text color
  //   });
  // }

  // addressEmptyAlert() {
  //   showMessage({
  //     message: "Address Field Empty",
  //     description: "Please Enter Address",
  //     type: "danger",
  //     icon: "danger",
  //     backgroundColor: "#f4c741", // background color
  //     color: "black", // text color
  //   });
  // }

  // phoneEmptyAlert() {
  //   showMessage({
  //     message: "Issue in Mobile Number Field",
  //     description: "Enter Mobile Number Correctly",
  //     type: "danger",
  //     icon: "danger",
  //     backgroundColor: "#f4c741", // background color
  //     color: "black", // text color
  //   });
  // }

  // emailEmptyAlert() {
  //   showMessage({
  //     message: "Issue in Email Field",
  //     description: "Please Enter Email Correctly",
  //     type: "danger",
  //     icon: "danger",
  //     backgroundColor: "#f4c741", // background color
  //     color: "black", // text color
  //   });
  // }

  // usernameEmptyAlert() {
  //   showMessage({
  //     message: "Username Field Empty",
  //     description: "Please Enter Username",
  //     type: "danger",
  //     icon: "danger",
  //     backgroundColor: "#f4c741", // background color
  //     color: "black", // text color
  //   });
  // }

  // passwordEmptyAlert() {
  //   showMessage({
  //     message: "Issue in Password Field",
  //     description: "Password should be at least 5 charactors",
  //     type: "danger",
  //     icon: "danger",
  //     backgroundColor: "#f4c741", // background color
  //     color: "black", // text color
  //   });
  // }

  // successAlert() {
  //   showMessage({
  //     message: "Sign up in Progress",
  //     description: "We sent a verification code to your phone",
  //     type: "success",
  //     icon: "success",
  //     backgroundColor: "#0a7334",
  //     color: "white", // text color
  //   });
  // }

  inputValidation() {
    const regMail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const regPhone = /^\d{8}/;

    if (this.state.fullName !== "") {
      if (this.state.address !== "") {
        if (this.state.phone !== "" && regPhone.test(this.state.phone) === true && this.state.phone.length < 9) {
          if (this.state.email !== "" && regMail.test(this.state.email) === true) {
            if (this.state.username !== "") {
              if (this.state.password !== "" && this.state.password.length > 4) {
                // Alert.alert("OK", "Perfect");
                this.checkUsernameExist();
               // this.successAlert();
               Alerts.FlashMessage(Strings.SuccessAlert,Strings.SuccessAlertDescription,Strings.AlertIcons[1],Colors.SuccessColor,Colors.AlertTextColor)
              } else {
                // Alert.alert("Error", "Password Empty");
               // this.passwordEmptyAlert();
               Alerts.FlashMessage(Strings.IssuePasswordAlert,Strings.IssuePasswordAlertDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.AlertTextColor)
              }
            } else {
              // Alert.alert("Error", "Username Empty");
              //this.usernameEmptyAlert();
              Alerts.FlashMessage(Strings.UserNameFieldAlert,Strings.UserNameFieldAlertDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.AlertTextColor)
            }
          } else {
            // Alert.alert("Error", "Enter Correct Email");
           // this.emailEmptyAlert();
           Alerts.FlashMessage(Strings.IssueEmail,Strings.IssueEmailDescription,Strings.AlertIcons[2],Colors.SuccessColor,Colors.AlertTextColor)
          }
        } else {
          // Alert.alert("Error", "Enter Phone Number Correctly");
         // this.phoneEmptyAlert();
         Alerts.FlashMessage(Strings.IssueMobileNumber,Strings.IssueMobileNumberDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.AlertTextColor)
        }
      } else {
        // Alert.alert("Error", "Address Empty");
        //this.addressEmptyAlert();
        Alerts.FlashMessage(Strings.AddressFieldAlert,Strings.AddressDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.AlertTextColor)
      }
    } else {
      // Alert.alert("Error", "Full Name Empty");
      //this.FullnameEmptyAlert();
      Alerts.FlashMessage(Strings.FullNameEmptyAlert,Strings.FullNameEmptyAlertDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.AlertTextColor)
    }

  }


  checkUsernameExist() {
    fetch(Strings.BaseUrl+'api/validate/checkusername?username=' + this.state.username, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then((response) => {
        console.log("RESPONSE:>>>" + JSON.stringify(response))
        return response.json()
      })
      .then((responseJson) => {
        console.log(responseJson);
        if (responseJson != true) {
          // Alert.alert("Username OK");
          this.registerClient();

        } else {
         // this.userNameExistAlert();
         Alerts.FlashMessage(Strings.UserNameAlreadyExistAlert,Strings.UserNameAlreadyExistAlertDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
          // Alert.alert("Username Already Exist", "Please Change your Username");
        }
        console.log("response - " + JSON.stringify(responseJson));
      })

      .catch((error) => {
        // Alert.alert("Failed !!", "Your Request Was Not Sent")
        console.log(error)
      });

  }

  registerClient() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected === true) {
        var data = JSON.stringify({
          "name": this.state.fullName,
          "address": this.state.address,
          "branchId": '01',
          "email": this.state.email,
          "mobile": '+974' + this.state.phone,
          "username": this.state.username,
          "password": this.state.password
        });

        console.log("data:" + data);

        fetch(Strings.BaseUrl+'api/client/registation', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: data
        })
          .then((response) => {
            console.log("RESPOSE:>>>" + JSON.stringify(response))
            return response.json()
          })
          .then((responseJson) => {
            this.setState({
              isLoading: false
            })
            // Alert.alert("Success", "Your Request Was Sent Successfully!")
            // this.props.navigation.navigate('NewDash');
            this.props.navigation.navigate('Verify', {
              navAppData: responseJson
            });
            console.log("response - " + JSON.stringify(responseJson));
          })

          .catch((error) => {
            // Alert.alert("Failed !!", "Your Request Was Not Sent")
            console.log(error)
          });


      } else {
        //this.errorMsgForConnection();
        Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
      }
    });

  }


  onClickListener() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected === true) {
        this.inputValidation();
      } else {
        //this.errorMsgForConnection();
        Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
      }
    });
  }


  // userNameExistAlert() {
  //   showMessage({
  //     message: "Username Already Exist",
  //     description: "Please Change Your Username",
  //     type: "warning",
  //     icon: "warning",
  //     backgroundColor: "#f4c741", // background color
  //     color: "black", // text color
  //   });
  // }

  // errorMsgForConnection() {
  //   showMessage({
  //     message: "Connection Failed",
  //     description: "You are not connect to the internet",
  //     type: "warning",
  //     icon: "warning",
  //     backgroundColor: "#ed1212", // background color
  //     color: "white", // text color
  //   });
  // }


  _onSelect = (item) => {
    // console.log(item.label);
    this.setState({
      selectedBranch: item.value,
    })
    // console.log(this.state.issueName);
  };

  render() {

    return (
      <ScrollView style={styles.scrollStyle} contentContainerStyle={{ flexGrow: 1 }}>

        <View style={styles.container}>
          <Image source={require('../assets/handy.png')} style={styles.HeadericonlImage2} />

          <View style={styles.inputContainer}>
            {/* <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/male-user/ultraviolet/50/3498db' }} /> */}
            <Image style={styles.inputIcon} source={require('../assets/avatar.png')} />
            <TextInput style={styles.inputs}
              placeholder="Full name"
              value={this.setState.fullName}
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(fullName) => this.setState({ fullName })}
            />
          </View>

          <View style={styles.inputContainer}>
            {/* <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/male-user/ultraviolet/50/3498db' }} /> */}
            <Image style={styles.inputIcon} source={require('../assets/home.png')} />
            <TextInput style={styles.inputs}
              placeholder="Address"
              value={this.setState.address}
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(address) => this.setState({ address })}
            />
          </View>

          <View style={styles.inputContainer}>
            <Image style={styles.inputIcon} source={require('../assets/sign_up_mobile.png')}
            />

            <Label style={styles.mobileLabel}>+974</Label>

            <TextInput style={styles.inputs}
              placeholder="Mobile"
              keyboardType="numeric"
              value={this.setState.phone}
              underlineColorAndroid='transparent'
              onChangeText={(phone) => this.setState({ phone })}
            />

          </View>

          <View style={styles.inputContainer}>
            <Image style={styles.inputIcon} source={require('../assets/mailIcon.png')} />
            <TextInput style={styles.inputs}
              placeholder="Email"
              value={this.setState.email}
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(email) => this.setState({ email })}
            />
          </View>
          <View style={styles.inputContainer}>
            <Image style={styles.inputIcon} source={require('../assets/username.png')} />
            <TextInput style={styles.inputs}
              placeholder="User Name"
              value={this.setState.username}
              underlineColorAndroid='transparent'
              onChangeText={(username) => this.setState({ username })}
            />
          </View>

          <View style={styles.inputContainer}>
            <Image style={styles.inputIcon} source={require('../assets/keyIcon.png')} />
            <TextInput style={styles.inputs}
              placeholder="Password"
              value={this.setState.password}
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => this.setState({ password })} />
          </View>

          <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.onClickListener()}>
            <Text style={styles.signUpText}>Sign up</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>


    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#F5FCFF',
    padding: 30
  },

  mobileLabel: {
    width: 5,
    flex: 0.275

  },


  scrollStyle: {
    // backgroundColor: '#F5FCFF',
  },

  HeadericonlImage2: {
    // padding: 3,
    width: 190,
    height: 90,
    marginBottom: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -10
  },

  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#848181',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },

  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#00b5ec',
    flex: 1,
  },

  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  signupButton: {
    backgroundColor: "#f4c741",
  },
  signUpText: {
    color: 'black',
  }
});