
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert,
    AsyncStorage,
    NetInfo
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../../essentials/Strings';
import Alerts from '../../essentials/Alerts';
import Colors from '../../essentials/Colors';

export default class FPW_EnterPinCode extends Component {

    constructor(props) {
        super(props);
        this.state = {
            clientID: null,
            pin: ''
        }
    }


    // pinCodeMatchedAlert() {
    //     showMessage({
    //         message: "Success!",
    //         description: "Pin Code Matched",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#05661a", // background color
    //         color: "white", // text color
    //     });
    // }

    // pinCodeNotFoundAlert() {
    //     showMessage({
    //         message: "Pin Code Error!",
    //         description: "Enter the Correct Pin Code",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    // errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }


    componentDidMount() {
        this._retrieveData();
    }


    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_ID');
            if (value !== null) {
                this.setState({
                    clientID: JSON.parse(value)
                })

                    .catch((error) => {
                        console.log(error)
                    });
            }

        } catch (error) {
            // Error retrieving data
            console.log(error);
        }
    }

    onClickListener = (viewId) => {

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BaseUrl+'api/client/' + this.state.clientID + '/password/change/verification?verifyCode=' + this.state.pin, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    }
                })
                    .then((response) => {
                        console.log("RES:>>>" + JSON.stringify(response));
                        return response.json()
                    })
                    .then((responseJson) => {
                        console.log("Response:>>>" + JSON.stringify(responseJson));
                        if (responseJson != false) {
                            // this.loginSuccessAlert();
                            // Alert.alert("Success!", "Pin Code Match !");
                            //this.pinCodeMatchedAlert();
                            Alerts.FlashMessage(Strings.pinCodeMatchedAlert,Strings.pinCodeMatchedAlertDescription,Strings.AlertIcons[2],Colors.SuccessColor,Colors.WarningTextColor)
                            this.props.navigation.navigate('FPW_ResetPW');
                            // this._storeData();
                        } else {
                            // Alert.alert("Failed", "Pin Code Didn't Match !");
                            // this.loginFailedAlert();
                            Alerts.FlashMessage(Strings.pinCodeNotFoundAlert,Strings.pinCodeNotFoundAlertDescription,Strings.AlertIcons[4],Colors.ErrorColor,Colors.WarningTextColor)
                        }
                    })
                    .catch((error) => {
                        // console.log(error)
                    });
            } else {
                // Alert.alert("Connection Failed", "Please check your Network Connection !");
                //this.errorMsgForConnection();
                Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
            }
        });
    }



    render() {
        return (
            <ScrollView style={styles.scrollStyle} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.container}>
                    <Image source={require('../../assets/handy.png')} style={styles.HeadericonlImage2} />

                    <Text style={styles.pinText}> Enter Pin Code </Text>

                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={require('../../assets/keyIcon.png')} />
                        <TextInput style={styles.inputs}
                            placeholder="PIN CODE"
                            value={this.setState.pin}
                            underlineColorAndroid='transparent'
                            onChangeText={(pin) => this.setState({ pin })}
                        />
                    </View>
                    {/* <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.props.navigation.navigate('FPW_ResetPW')}>
                        <Text style={styles.signUpText}>Next</Text>
                    </TouchableHighlight> */}

                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.onClickListener('pin')}>
                        <Text style={styles.signUpText}>Next</Text>
                    </TouchableHighlight>


                </View>
            </ ScrollView>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },

    pinText: {
        padding: 10
    },


    scrollStyle: {
        backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        width: 310,
        height: 150,
        marginBottom: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#848181',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#00b5ec',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },
    signupButton: {
        backgroundColor: "#f4c741",
    },
    signUpText: {
        color: 'black',
    }
});