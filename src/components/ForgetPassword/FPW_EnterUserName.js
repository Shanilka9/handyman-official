
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert,
    AsyncStorage,
    NetInfo
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../../essentials/Strings';
import Alerts from '../../essentials/Alerts';
import Colors from '../../essentials/Colors';

export default class FPW_EnterUserName extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            clientId: ''

        }
    }

    // usernameMatchedAlert() {
    //     showMessage({
    //         message: "Success!",
    //         description: "Username Exsist",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#05661a", // background color
    //         color: "white", // text color
    //     });
    // }

    // usernameNotFoundAlert() {
    //     showMessage({
    //         message: "Failed!",
    //         description: "Username not Exsist",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    // errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    onClickListener = (viewId) => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BaseUrl+'api/client/passowrd/forget?username=' + this.state.username, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        // 'Authorization': 'Basic ' + base64.encode(this.props.navigation.state.params.navAppData.username + ":" + this.props.navigation.state.params.navAppData.password)
                    }
                })
                    .then((response) => {
                        console.log("RES:>>>" + JSON.stringify(response));
                        return response.json()
                    })
                    .then((responseJson) => {
                        console.log("Response:>>>" + JSON.stringify(responseJson.status));
                        console.log("Response:>>>" + JSON.stringify(responseJson.id));
                        // console.log("Response:>>>" + JSON.stringify(responseJson.status));
                        if (responseJson.status != false) {

                            this.setState({
                                clientId: responseJson.id
                            })

                            console.log("ClientID :>" + this.state.clientId);


                            // this.loginSuccessAlert();
                            // Alert.alert("Success", "Username Match !");
                            //this.usernameMatchedAlert();
                            Alerts.FlashMessage(Strings.usernameMatchedAlert,Strings.usernameMatchedAlertDescription,Strings.AlertIcons[1],Colors.SuccessColor,Colors.WarningTextColor)


                            this._storeData();
                        } else {
                            // Alert.alert("Username Error!", "Username Unavailable");
                            //this.usernameNotFoundAlert();
                            Alerts.FlashMessage(Strings.usernameNotFoundAlert,Strings.usernameNotFoundAlertDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
                        }
                    })
                    .catch((error) => {
                        // console.log(error)
                    });
            } else {
                // Alert.alert("Connection Failed", "Please check your Network Connection !");
                //this.errorMsgForConnection();
                Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
            }
        });
    }

    _storeData = async () => {
        try {
            await AsyncStorage.setItem('client_ID', JSON.stringify(this.state.clientId), () => {
                this.props.navigation.navigate('FPW_EnterPin');
            });

        } catch (error) {
            console.error(error);
        }
    }


    render() {
        return (
            <ScrollView style={styles.scrollStyle} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.container}>
                    <Image source={require('../../assets/handy.png')} style={styles.HeadericonlImage2} />

                    <Text style={styles.pinText}> Enter Your Username Correctly </Text>

                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={require('../../assets/avatar.png')} />
                        <TextInput style={styles.inputs}
                            placeholder="USER NAME"
                            value={this.setState.username}
                            underlineColorAndroid='transparent'
                            onChangeText={(username) => this.setState({ username })}
                        />
                    </View>

                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.onClickListener('username')}>
                        <Text style={styles.signUpText}>Next</Text>
                    </TouchableHighlight>


                </View>
            </ ScrollView>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },

    pinText: {
        padding: 10
    },


    scrollStyle: {
        backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        width: 310,
        height: 150,
        marginBottom: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#848181',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#00b5ec',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },
    signupButton: {
        backgroundColor: "#f4c741",
    },
    signUpText: {
        color: 'black',
    }
});