import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert,
    AsyncStorage,
    NetInfo
} from 'react-native';
import { Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import base64 from 'react-native-base64';
import Strings from '../../essentials/Strings';
import Alerts from '../../essentials/Alerts';
import Colors from '../../essentials/Colors';

export default class FPW_ResetPW extends Component {

    constructor(props) {
        super(props);
        this.state = {
            clientID: 'null',
            newPassword: '',
            newPasswordX: ''

        }
    }


    // passwordResetAlert() {
    //     showMessage({
    //         message: "Success!",
    //         description: "Password Changed Successfully",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#05661a", // background color
    //         color: "white", // text color
    //     });
    // }

    // passwordMismatchAlert() {
    //     showMessage({
    //         message: "Password Mismatch!",
    //         description: "Re-Enter the New Password Correctly",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    // errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    componentDidMount() {
        this._retrieveData();
    }


    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_ID');
            if (value !== null) {
                this.setState({
                    clientID: JSON.parse(value)
                })

                    .catch((error) => {
                        console.log(error)
                    });
            }

        } catch (error) {
            // Error retrieving data
            console.log(error);
        }
    }

    passwordValidator() {
        if (this.state.newPassword == this.state.newPasswordX) {
            // Alert.alert("Success!", "Password has been Changed Successfully");
           // this.passwordResetAlert();
            this.passwordReset();
            Alerts.FlashMessage(Strings.passwordResetAlert,Strings.passwordResetAlertDescription,Strings.AlertIcons[1],Colors.SuccessColor,Colors.WarningTextColor)

        } else {
            //this.passwordMismatchAlert();
            // Alert.alert("Password Didn't Matched", "Re-Enter the New Password Correctly");
            Alerts.FlashMessage(Strings.passwordMismatchAlert,Strings.passwordMismatchAlertDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)

        }
    }


    passwordReset() {
        fetch(Strings.BaseUrl+'api/client/' + this.state.clientID + '/passowrd/change?password=' + this.state.newPasswordX, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                // 'Authorization': 'Basic ' + base64.encode(this.state.clientData)
            }
        })
            .then((response) => {
                console.log("RES:>>>" + JSON.stringify(response));
                return response.json()
            })
            .then((responseJson) => {
                console.log("Response :>>>" + JSON.stringify(responseJson));
                console.log("Status :>>>" + JSON.stringify(responseJson.status));
                if (responseJson.status != false) {
                    // this.loginSuccessAlert();
                    // Alert.alert("Success!", "Password Changed Successfully");
                    this.props.navigation.navigate('SignIn');
                    // this._storeData();

                } else {
                    // Alert.alert("Failed!", "Unable to Change Password");
                    // this.loginFailedAlert();
                }
            })
            .catch((error) => {
                // console.log(error)
            });

    }


    onClickListener = (viewId) => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                this.passwordValidator();
            } else {
                // Alert.alert("Connection Failed", "Please check your Network Connection !");
                //this.errorMsgForConnection();
                Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
            }
        });
    }

    render() {
        return (
            <ScrollView style={styles.scrollStyle} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.container}>
                    <Image source={require('../../assets/handy.png')} style={styles.HeadericonlImage2} />

                    <Text style={styles.pinText}> Enter New Password </Text>

                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={require('../../assets/keyIcon.png')} />
                        <TextInput style={styles.inputs}
                            placeholder="New Password"
                            value={this.setState.newPassword}
                            underlineColorAndroid='transparent'
                            onChangeText={(newPassword) => this.setState({ newPassword })}
                        />
                    </View>

                    <Text style={styles.pinText}> Re-Enter New Password </Text>

                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={require('../../assets/keyIcon.png')} />
                        <TextInput style={styles.inputs}
                            placeholder="Password"
                            value={this.setState.newPasswordX}
                            underlineColorAndroid='transparent'
                            onChangeText={(newPasswordX) => this.setState({ newPasswordX })}
                        />
                    </View>

                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.onClickListener('newpw')}>
                        <Text style={styles.signUpText}>Submit</Text>
                    </TouchableHighlight>

                </View>
            </ ScrollView>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },

    pinText: {
        padding: 10
    },


    scrollStyle: {
        backgroundColor: '#F5FCFF',
    },
    HeadericonlImage2: {
        width: 310,
        height: 150,
        marginBottom: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#848181',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#00b5ec',
        flex: 1,
    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },
    signupButton: {
        backgroundColor: "#f4c741",
    },
    signUpText: {
        color: 'black',
    }
});