import React, { Component } from 'react';
import { Container, Header, Content, Footer, ListItem, Text, Radio, Right, Left, Icon, Title, Body, Button, Subtitle, Card, CardItem, } from 'native-base';
import { AsyncStorage, StyleSheet, View, ActivityIndicator, BackHandler, NetInfo, Alert } from 'react-native';
import base64 from 'react-native-base64';
import RadioForm from 'react-native-radio-form';
import Spinner from 'react-native-loading-spinner-overlay';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

export default class ServiceIssue extends Component {


    constructor(props) {
        super(props);

        this.state = {
            radioDataSourse: [],
            tempRadioDataSourse: [],
            username: "",
            password: "",
            clientData: null,
            isLoading: true,
            categoryId: 0,
            categoryName: "",
            issueName: "sample",
            appData: {},
            spinner: false
        };
    }

    handleBackPress = () => {
        BackHandler.exitApp();
        console.log("BACKPRESS")
        // this.props.navigation.goBack()
        return true;
    }

    componentWillMount() {

        this.setState({
            spinner: true
        })
        // this.setState({
        //     categoryId: this.props.navigation.state.params.viewId,
        //     categoryName: this.props.navigation.state.params.viewName
        // })

        this.setState({
            appData: this.props.navigation.state.params.navAppData
        })

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                this._retrieveData();
            } else {
                //this.errorMsgForConnection();
                // Alert.alert("Error", "No Internet Connection")
                Alerts.FlashMessage(Strings.ConnectionErrorMessage, Strings.ConnectionErrorDescription, Strings.AlertIcons[2], Colors.ErrorColor, Colors.WarningTextColor)
            }
        });

        // this._retrieveData();
    }

    // errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_data');
            if (value !== null) {
                this.setState({
                    clientData: JSON.parse(value)
                })

                return fetch(Strings.BaseUrl + 'api/app/client/category/' + this.state.appData.categoryId, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password)
                    }
                })

                    .then((response) => response.json())
                    .then((responseJson) => {

                        // console.log("response - " + JSON.stringify(responseJson));
                        // console.log("response - " + JSON.stringify(responseJson.itemIssues));

                        this.setState({
                            radioDataSourse: responseJson.itemIssues,
                            spinner: false
                        })
                        // console.log("Issues - " + this.state.radioDataSourse);
                        for (var i = 0; i < this.state.radioDataSourse.length; i++) {
                            this.state.tempRadioDataSourse.push({
                                label: this.state.radioDataSourse[i],
                                // value: 'gg'
                            })
                        }
                        this.setState({
                            issueName: this.state.tempRadioDataSourse[0].value,

                        })
                        this.state.appData.categoryIssue = this.state.tempRadioDataSourse[0].label
                        this.setState({
                            isLoading: false,
                            spinner: false
                        })

                    })

                    .catch((error) => {
                        console.log(error)

                    });
            }
            // console.log(this.state.clientData);
            // console.log(this.responseJson)
        } catch (error) {
            // Error retrieving data
            console.log(error);


        }
    }

    componentDidMount() {
        // this._retrieveData();
    }


    _onSelect = (item) => {
        // console.log(item.label);
        this.setState({
            issueName: item.label,

        })
        this.state.appData.categoryIssue = item.label
        // console.log(this.state.issueName);
    };


    _storeData = async () => {
        try {
            await AsyncStorage.setItem('category_issues', JSON.stringify(this.state.categoryId), JSON.stringify(this.state.categoryName), JSON.stringify(this.state.issueName), () => {
                // Alert.alert("Login Success");
                this.props.navigation.navigate('Details');

            });

        } catch (error) {
            console.error(error);
        }
    }

    onClickListener = (viewId) => {
        this.setState({
            Spinner: true
        })

        this.props.navigation.navigate('Details', {
            navAppData: this.state.appData
        });
        // console.log(this.state.issueName);
        // this._onSelect();
        // console.log(this.state.categoryId + " - " + this.state.categoryName + " - " + this.state.issueName);
        // console.log(this.state.issueName);       
    }

    render() {



        if (this.state.isLoading) {
            // this._retrieveData();
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size='large' color='#f0ad4e' />
                </View>
            )
        } else {
            return (

                <Container>

                    <Header style={styles.headerStyle}>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.goBack()}>

                                <Icon name='arrow-back' />
                            </Button>
                        </Left>
                        <Body style={styles.bodyStyle}>
                            <Title style={styles.headerStyles}>Issue</Title>

                        </Body>

                        <Right>
                            <Button transparent>
                                <Text>Cancel</Text>
                            </Button>
                        </Right>
                    </Header>




                    <Content>


                        <Spinner
                            visible={this.state.spinner}
                            textContent={'Loading...'}
                        />


                        <View style={styles.container}>
                            <View style={{ marginHorizontal: 10 }} >
                                <RadioForm
                                    style={{ width: 350 - 30 }}
                                    dataSource={this.state.tempRadioDataSourse}
                                    itemShowKey="label"
                                    itemRealKey="value"
                                    circleSize={18}
                                    initial={this.state.tempRadioDataSourse[0].value}
                                    formHorizontal={false}
                                    labelHorizontal={true}
                                    onPress={(item) => this._onSelect(item)}
                                    outerColor={Colors.nativeColor}
                                    innerColor={Colors.nativeColor}
                                />

                            </View>
                        </View>




                    </Content>

                    <Footer style={styles.footerStyles}>
                        <Button transparent style={styles.nextButton} full onPress={() => this.onClickListener('issue')}>
                            <Text style={styles.nbc}>NEXT</Text>

                        </Button>


                    </Footer>
                </Container>
            );
        }



    }
}


const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },
    loadingContainer: {
        flex: 1,
        justifyContent: 'center'
    },


    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18
    },

    headerStyle: {
        backgroundColor: '#f0ad4e'
    },

    footerStyles: {
        backgroundColor: '#f0ad4e',
        alignItems: 'center'
    },

    nextButton: {
        width: 300,
        height: 55,
        alignItems: 'center',
        backgroundColor: '#f0ad4e',

    },

    nbc: {
        color: 'black',
        fontSize: 16
    },



    headerText: {
        color: 'white'
    },
    areaTopic: {
        fontWeight: 'bold'


    },

    timeStyles: {
        fontSize: 14,
        color: 'black'
    },

    subStyle: {
        color: 'black',
        fontSize: 12
    },


    bodyStyle: {
        flex: 1,
        flexDirection: 'column',

        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

    areaStyles: {
        fontSize: 13,
        color: 'black'
    },

});