import React, { Component } from 'react';
import { Container, Form, Item, Input, Header, Content, Footer, ListItem, Text, Radio, Right, Left, Icon, Title, Body, Button, Subtitle, Label, Card, CardItem } from 'native-base';
import { View, StyleSheet, Image, TextInput, Picker, Alert, NetInfo } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

export default class ProviderRequest extends Component {



    constructor(props) {
        super(props);

        this.state = {
            providerName: '',
            address: '',
            areaId: '',
            categoryId: '',
            contactPersonNumber: '',
            contactPersonName: '',
            email: '',
            mobile: '',

            categoryData: [],
            areaData: [],

        }
    }

    componentWillMount() {
        this.loadCategories();
        this.loadAreas();
    }

    //-----------------Alerts----------------------------------

    // providerNameEmptyAlert() {
    //     showMessage({
    //         message: "Provider Name Field Empty",
    //         description: "Please Enter Provider Name",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // addressEmptyAlert() {
    //     showMessage({
    //         message: "Address Field Empty",
    //         description: "Please Enter Address",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // phoneEmptyAlert() {
    //     showMessage({
    //         message: "Issue in Mobile Number Field",
    //         description: "Enter Mobile Number Correctly",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // emailEmptyAlert() {
    //     showMessage({
    //         message: "Issue in Email Field",
    //         description: "Please Enter Email Correctly",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // categoryEmptyAlert() {
    //     showMessage({
    //         message: "No Category Selected",
    //         description: "Please Select Category",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // areaEmptyAlert() {
    //     showMessage({
    //         message: "No Area Selected",
    //         description: "Please Select Area",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // contactPersonEmptyAlert() {
    //     showMessage({
    //         message: "Contact Person Name Field Empty",
    //         description: "Please Enter Contact Person Name",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // contactPersonNumberEmptyAlert() {
    //     showMessage({
    //         message: "Issue in Contact Person Mobile Field",
    //         description: "Please Enter Contact Person Mobile Correctly",
    //         type: "danger",
    //         icon: "danger",
    //         backgroundColor: "#f4c741", // background color
    //         color: "black", // text color
    //     });
    // }

    // errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    // requestSentAlert() {
    //     showMessage({
    //         message: "Success",
    //         description: "Your Request Sent Successfully",
    //         type: "success",
    //         icon: "success",
    //         backgroundColor: "#0a7334", // background color
    //         color: "white", // text color
    //     });
    // }~

    // requestFailedAlert() {
    //     showMessage({
    //         message: "Failed",
    //         description: "Unable to Send Your Request",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }

    //--------------------------------------------------
    onClickListener() {
        // this.inputValidator();
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                //   Alert.alert("Press")
                // this.uplaodData();
                this.inputValidation();
            } else {
                Alerts.FlashMessage(Strings.ConnectionErrorMessage,Strings.ConnectionErrorDescription,Strings.AlertIcons[2],Colors.ErrorColor,Colors.WarningTextColor)
            }
        });
    }


    inputValidation() {
        const regMail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const regPhone = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;

        if (this.state.providerName !== "") {
            if (this.state.address !== "") {
                if (this.state.mobile !== "" && regPhone.test(this.state.mobile) === true) {
                    if (this.state.email !== "" && regMail.test(this.state.email) === true) {
                        if (this.state.categoryId !== "") {
                            if (this.state.areaId !== "") {
                                if (this.state.contactPersonName !== "") {
                                    if (this.state.contactPersonNumber !== "" && regPhone.test(this.state.contactPersonNumber) === true) {
                                        // Alert.alert("OK", "Perfect");
                                        this.uplaodData();

                                    } else {
                                        // Alert.alert("Error", "Enter Contact Person Phone Number Correctly");
                                        // this.contactPersonNumberEmptyAlert();
                                        Alerts.FlashMessage(Strings.IssueContactPersonMobile,Strings.IssueContactPersonMobileDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)

                                    }
                                } else {
                                    // Alert.alert("Error", "Contact Person Empty");
                                   // this.contactPersonEmptyAlert();
                                   Alerts.FlashMessage(Strings.ContactPersonNameField,Strings.ContactPersonNameFieldDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
                                }
                            } else {
                                // Alert.alert("Error", "Area Empty");
                               // this.areaEmptyAlert();
                               Alerts.FlashMessage(Strings.NoAreaSelected,Strings.NoAreaSelectedDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
                            }
                        } else {
                            // Alert.alert("Error", "Category Empty");
                            //this.categoryEmptyAlert();
                            Alerts.FlashMessage(Strings.NoCategorySelected,Strings.NoCategorySelectedDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
                        }
                    } else {
                        // Alert.alert("Error", "Enter Correct Email");
                       // this.emailEmptyAlert();
                       Alerts.FlashMessage(Strings.IssueEmail,Strings.IssueEmailDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
                    }
                } else {
                    // Alert.alert("Error", "Enter Phone Number Correctly");
                   // this.phoneEmptyAlert();
                    Alerts.FlashMessage(Strings.IssueMobileNumber,Strings.IssueMobileNumberDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
                }
            } else {
                // Alert.alert("Error", "Address Empty");
            //    this.addressEmptyAlert();
            Alerts.FlashMessage(Strings.AddressFieldAlert,Strings.AddressDescription,Strings.AlertIcons[4],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
            }
        } else {
            // Alert.alert("Error", "Provider Name Empty");
            Alerts.FlashMessage(Strings.ProviderNameFieldAlert,Strings.ProviderNameDescription,Strings.AlertIcons[2],Colors.WarningAlertbackgroundColor,Colors.WarningTextColor)
        }

    }


    loadCategories() {
        return fetch(Strings.BaseUrl+'api/app/category', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })

            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    categoryData: responseJson
                })
                console.log("CategoryData:>:>:>" + JSON.stringify(this.state.categoryData));

            })

            .catch((error) => {
                console.log(error)
            });
    }

    loadCategoryTypes() {
        return this.state.categoryData.map(data => (
            <Picker.Item label={data.name} value={data.id} key={data.id} />
        ))
    }

    loadAreas() {
        return fetch(Strings.BaseUrl+'api/app/branch/1/area', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })

            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    areaData: responseJson
                })

                console.log("AreaData:>:>:>" + JSON.stringify(this.state.areaData));


            })
            .catch((error) => {
                console.log(error)
            });

    }

    loadAreaTypes() {
        return this.state.areaData.map(data => (
            <Picker.Item label={data.name} value={data.id} key={data.id} />
        ))
    }



    uplaodData() {
        var data = JSON.stringify({
            "name": this.state.providerName,
            "address": this.state.address,
            "areaId": this.state.areaId,
            "categoryId": this.state.categoryId,
            "contactPersonNumber": this.state.contactPersonNumber,
            "contactPersonName": this.state.contactPersonName,
            "email": this.state.email,
            "mobile": this.state.mobile

        });

        fetch(Strings.BaseUrl+'api/provider/registation', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: data
        })
            .then((response) => response.json())
            .then((responseJson) => {

                // Alert.alert("Success", "Your Request Was Sent Successfully!")
               // this.requestSentAlert();
               Alerts.FlashMessage(Strings.RequestSuccessAlert,Strings.RequestSuccessDescription,Strings.AlertIcons[4],Colors.SuccessColor,Colors.WarningTextColor)
                this.props.navigation.navigate('NewDash');
                // console.log("response - " + JSON.stringify(responseJson));
            })

            .catch((error) => {
                // Alert.alert("Failed !!", "Your Request Was Not Sent")
               // this.requestFailedAlert();
               Alerts.FlashMessage(Strings.RequestFailedAlert,Strings.RequestFailedDescription,Strings.AlertIcons[4],Colors.ErrorColor,Colors.WarningTextColor)
                console.log(error)
            });
    }

    render() {
        const { headerStyles, footerStyles, nbc, headerText, nextButton, areaTopic, areaStyles, progressStyle, headerStyle, subStyle, bodyStyle } = styles
        return (
            <Container>

                {/* header strting here */}

                <Header style={styles.headerStyle}>


                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body style={styles.bodyStyle}>
                        <Title style={headerStyles}>Provider Request</Title>

                    </Body>
                </Header>

                {/* Header close!!!!!!!!!!!!! */}


                <Content padder>

                    <ScrollView style={styles.scrollStyle} contentContainerStyle={{ flexGrow: 1 }}>

                        <View style={styles.container}>
                            {/* <Image source={require('../assets/handy.png')} style={styles.HeadericonlImage2} /> */}

                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/avatar.png')} />
                                <TextInput style={styles.inputs}
                                    placeholder="Provider Name"
                                    value={this.setState.providerName}
                                    keyboardType="email-address"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(providerName) => this.setState({ providerName })}
                                />
                            </View>

                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/home.png')} />
                                <TextInput style={styles.inputs}
                                    placeholder="Address"
                                    value={this.setState.address}
                                    keyboardType="email-address"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(address) => this.setState({ address })}
                                />
                            </View>

                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/sign_up_mobile.png')}
                                />
                                <TextInput style={styles.inputs}
                                    placeholder="Mobile"
                                    keyboardType="numeric"
                                    value={this.setState.mobile}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(mobile) => this.setState({ mobile })}
                                />
                            </View>


                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/mailIcon.png')} />
                                <TextInput style={styles.inputs}
                                    placeholder="Email"
                                    value={this.setState.email}
                                    keyboardType="email-address"
                                    underlineColorAndroid='transparent'
                                    onChangeText={(email) => this.setState({ email })}
                                />
                            </View>

                            {/* ------------------------------------------Category List------------------------------------------------- */}
                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/branch.png')} />
                                <Picker
                                    style={{ height: 20, width: 230 }}
                                    selectedValue={this.state.categoryId}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ categoryId: itemValue })}
                                >
                                    <Picker.Item label="- Select Category -" value="" />
                                    {this.loadCategoryTypes()}
                                </Picker>
                            </View>

                            {/* ------------------------------------------Area List------------------------------------------------- */}


                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/branch.png')} />
                                <Picker
                                    style={{ height: 20, width: 230 }}
                                    selectedValue={this.state.areaId}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ areaId: itemValue })}
                                >
                                    <Picker.Item label="- Select Area -" value="" />
                                    {this.loadAreaTypes()}
                                </Picker>
                            </View>

                            {/* ------------------------------------------------------------------------------------------- */}

                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/username.png')} />
                                <TextInput style={styles.inputs}
                                    placeholder="Contact Person Name"
                                    value={this.setState.contactPersonName}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(contactPersonName) => this.setState({ contactPersonName })}
                                />
                            </View>

                            <View style={styles.inputContainer}>
                                <Image style={styles.inputIcon} source={require('../assets/sign_up_mobile.png')} />
                                <TextInput style={styles.inputs}
                                    placeholder="Contact Person Mobile"
                                    keyboardType="numeric"
                                    value={this.setState.contactPersonNumber}
                                    underlineColorAndroid='transparent'
                                    onChangeText={(contactPersonNumber) => this.setState({ contactPersonNumber })} />
                            </View>

                        </View>
                    </ScrollView>





                </Content>



                {/* footer start */}

                <Footer style={footerStyles}>

                    {/* buttton for navigete to next page */}

                    <Button transparent style={nextButton} full onPress={() => this.onClickListener()} >
                        <Text style={nbc}>Submit</Text>

                    </Button>

                </Footer>

                {/* footer end */}

            </Container>
        );
    }
}

const styles = StyleSheet.create({


    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18,

    },


    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
        padding: 30
    },

    scrollStyle: {
        // backgroundColor: '#F5FCFF',
    },

    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#848181',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 270,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },

    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#00b5ec',
        flex: 1,
    },

    progressStyle: {
        height: 6,
        marginTop: 0,
        backgroundColor: 'black',
        width: 60

    },

    footerStyles: {
        backgroundColor: '#f0ad4e',
        alignItems: 'center'
    },

    headerText: {
        color: 'white'
    },
    areaTopic: {
        fontWeight: 'bold'
    },

    areaStyles: {
        fontSize: 13,
        color: 'black'
    },
    headerStyle: {
        backgroundColor: '#f0ad4e'
    },
    subStyle: {
        color: 'black',
        fontSize: 12
    },
    bodyStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: -120


    },

    nextButton: {
        width: 300,
        height: 55,
        alignItems: 'center',
        backgroundColor: '#f0ad4e',

    },

    nbc: {
        color: 'black',
        fontSize: 16
    }




});