import React, { Component } from 'react';
import {
    Container,
    Header,
    Footer,
    Body,
    Text,
    DatePicker,
    Right,
    Left,
    Icon,
    Title,
    Button,
    Textarea,
    Card,
    CardItem,
    Content,
    Item, Input, Label

} from 'native-base';
import { TouchableOpacity, Alert, View, StyleSheet, Image, ListView, BackHandler, NetInfo } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-crop-picker';
import RadioForm from 'react-native-radio-form';
import Moment from 'moment';
import CompressImage from 'react-native-compress-image';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import Strings from '../essentials/Strings';
import Alerts from '../essentials/Alerts';
import Colors from '../essentials/Colors';

const options = {
    title: 'Select Option',
    takePhotoButtonTitle: 'Take photo with your camera',
    chooseFromLibraryButtonTitle: 'Choose photo from library',
}


const mockData = [
    {
        label: 'Morning',
        value: 'MORNING'
    },
    {
        label: 'Noon',
        value: 'NOON'
    },
    {
        label: 'Night',
        value: 'NIGHT'
    }
];

export default class ServiceDetails extends Component {

    _onSelect = (item) => {
        this.setState({
            time: item.value
        })


    };

    constructor(props) {
        super(props);
        this.state = { chosenDate: new Date() };
        this.setDate = this.setDate.bind(this);
        this.state = {
            isVisible: false,
            avatarSource: null,
            image: null,
            images: null,
            appData: {},
            description: "",
            date: "",
            time: ""

        }

    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }


    componentWillMount() {
        this.setState({
            appData: this.props.navigation.state.params.navAppData
        })
        // console.log("app Data", this.state.appData.categoryId)
    }

    componentDidMount() {
        // console.log("app Data", this.state.appData.categoryId)

        this.setState({
            time: mockData[0].value
        })
    }



    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('client_data');
            if (value !== null) {
                this.setState({
                    clientData: JSON.parse(value)
                })

                return fetch(Strings.BaseUrl + 'api/app/client/job', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(this.state.clientData.username + ":" + this.state.clientData.password)
                    }
                })

                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            dataSourse: responseJson.categorys
                        })
                        // console.log("fdf" + this.state.dataSourse[0]);
                    })

                    .catch((error) => {
                        console.log(error)
                    });
            }
            // console.log(this.state.clientData);
            // console.log(this.responseJson)
        } catch (error) {
            // Error retrieving data
            console.log(error);


        }
    }


    // dateValidation() {
    //     showMessage({
    //         message: "Select Date",
    //         description: "No Date Selected",
    //         type: "default",
    //         backgroundColor: "#f4c741", // background color
    //         color: "white", // text color
    //     });
    // }

    // imageValidation() {
    //     showMessage({
    //         message: "Upload Image",
    //         description: "No Image Uploaded",
    //         type: "default",
    //         backgroundColor: "#f4c741", // background color
    //         color: "white", // text color
    //     });
    // }


    textValidation() {
        if (this.state.date == "") {
            // Alert.alert("Validate", "Please Select Date");
            //this.dateValidation();
            Alerts.FlashMessage(Strings.SelectdateAlert, Strings.SelectDateDescription, Strings.AlertIcons[5], Colors.WarningAlertbackgroundColor, Colors.AlertTextColor)
            return;
        } else if (this.state.image === null) {
            // Alert.alert("Validate", "Please Upload Image of your Issue");
            //this.imageValidation();
            Alerts.FlashMessage(Strings.UploadImage, Strings.UploadImageDescription, Strings.AlertIcons[5], Colors.WarningAlertbackgroundColor, Colors.AlertTextColor)
            return;
        } else {
            this.props.navigation.navigate('Area', {
                navAppData: this.state.appData
            });
            return;
        }

    }

    onClickListener = (viewId) => {
        // this.props.navigation.navigate('Area')
        // console.log("desc", this.state.description);
        // console.log("date", this.state.date);
        // console.log("time", this.state.time);
        this.state.appData.description = this.state.description;
        this.state.appData.image = this.state.image;
        this.state.appData.date = this.state.date;
        this.state.appData.time = this.state.time;
        // this.props.navigation.navigate('Area', {
        //     navAppData:this.state.appData
        // });


        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                this.textValidation();
            } else {
                //this.errorMsgForConnection();
                // Alert.alert("Error", "No Internet Connection")
                Alerts.FlashMessage(Strings.ConnectionErrorMessage, Strings.ConnectionErrorDescription, Strings.AlertIcons[2], Colors.ErrorColor, Colors.WarningTextColor)
            }
        });

        // this.textValidation();


    }

    // errorMsgForConnection() {
    //     showMessage({
    //         message: "Connection Failed",
    //         description: "You are not connect to the internet",
    //         type: "warning",
    //         icon: "warning",
    //         backgroundColor: "#ed1212", // background color
    //         color: "white", // text color
    //     });
    // }




    setDate(newDate) {
        this.setState({
            // chosenDate: newDate 
            date: Moment(newDate).format('YYYY-M-D')

        });
    }

    handlePicker = () => {
        this.setState({
            isVisible: false
        })
    }


    hidePicker = (date) => {
        this.setState({
            isVisible: false

        })
    }


    showPicker = () => {
        this.setState({
            isVisible: true
        })
    }


    pickMultiple() {
        ImagePicker.openPicker({
            multiple: true,
            waitAnimationEnd: false,
            includeExif: true,
            forceJpg: true,
        }).then(images => {
            this.setState({
                image: null,
                images: images.map(i => {
                    console.log('received image', i);
                    return { uri: i.path, width: i.width, height: i.height, mime: i.mime };
                })
            });
            console.log("IMAGES:" + JSON.stringify(this.state.images));
        }).catch(e => alert(e));
    }
    renderAsset(image) {
        if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
            return this.renderVideo(image);
        }

        return this.renderImage(image);
    }
    renderImage(image) {
        return <Image style={{
            flex: 1, width: 328, height: 210, resizeMode: 'contain', alignItems: 'center', flexDirection: 'row',
            justifyContent: 'center',
        }} source={image} />
    }
    renderVideo(video) {
        return (<View style={{ height: 300, width: 300 }}>
            <Video source={{ uri: video.uri, type: video.mime }}
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0
                }}
                rate={1}
                paused={false}
                volume={1}
                muted={false}
                resizeMode={'cover'}
                onError={e => console.log(e)}
                onLoad={load => console.log(load)}
                repeat={true} />
        </View>);
    }

    pickSingle() {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: false,
            cropperCircleOverlay: false,
            compressImageMaxWidth: 640,
            compressImageMaxHeight: 480,
            compressImageQuality: 0.5,
            compressVideoPreset: 'MediumQuality',
            includeExif: true,
        }).then(image => {
            console.log('received image', image);
            this.setState({
                image: { uri: image.path, width: image.width, height: image.height, mime: image.mime },
                images: null
            });
            this.compress();
        }).catch(e => {
            console.log(e);
            Alert.alert(e.message ? e.message : e);
        });
    }

    pickSingleWithCamera(cropping) {
        ImagePicker.openCamera({
            cropping: cropping,
            width: 500,
            height: 500,
            includeExif: true,
        }).then(image => {
            console.log('received image', image);
            this.setState({
                image: { uri: image.path, width: image.width, height: image.height },
                images: null
            });
            this.compress();
        }).catch(e => alert(e));
    }

    compress() {
        CompressImage.createCompressedImage(this.state.image.uri, 'Compress/Images')
            .then((response) => {
                this.state.image.uri = response.uri;
                this.state.image.path = response.path;
                this.state.image.mime = response.mime;
            }).catch((err) => {
                console.log(err);
                return Alert.alert('Unable to compress photo, Check the console for full the error message');
            });
    }


    render() {
        //rendering styles for the components.must be called once, cannot be duplicated

        const { headerStyles, nextButton, nbc, headerStyle, bodyStyle, subStyle, footerStyles, datePickerStyle, btnImage, areaStyles, headerText, areaTopic, timeStyles, grid1Style, progressStyle } = styles
        return (


            <Container>

                {/* --------------------------------------------------- */}

                <Header style={headerStyle}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>

                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body style={bodyStyle}>
                        <Title style={headerStyles}>Details</Title>

                    </Body>

                    <Right>
                        <Button transparent>
                            <Text>Cancel</Text>
                        </Button>
                    </Right>
                </Header>





                {/* <Content> */}
                <ScrollView>

                    <Content padder>
                        <Card>
                            <CardItem bordered>
                                <Body>
                                    {/* <Label>Service Date </Label> */}
                                    <TouchableOpacity style={{ width: 158, flex: 1, flexDirection: 'row', justifyContent: 'center', backgroundColor: '#f0ad4e', marginLeft: 65 }}>

                                        {/* <Label style={{width: 30}}></Label> */}

                                        <DatePicker
                                            defaultDate={new Date()}
                                            minimumDate={new Date()}
                                            locale={"en"}
                                            formatChosenDate={date => { return Moment(date).format('MMMM Do YYYY'); }}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            alignItems={'center'}
                                            placeHolderText=" Select Date"
                                            textStyle={{ color: "#000000" }}
                                            placeHolderTextStyle={{ color: "#000000" }}
                                            onDateChange={this.setDate}
                                        />


                                    </TouchableOpacity>

                                </Body>
                            </CardItem>

                            <CardItem bordered>
                                <Body>
                                    <Label style={{ flex: 1, marginLeft: 95, color: '#f0ad4e' }}> Select Time </Label>

                                    <RadioForm
                                        style={{ width: 350 - 30, marginLeft: 30 }}
                                        dataSource={mockData}
                                        itemShowKey="label"
                                        itemRealKey="value"
                                        circleSize={18}
                                        initial={0}
                                        formHorizontal={true}
                                        labelHorizontal={true}
                                        outerColor={Colors.nativeColor}
                                        innerColor={Colors.nativeColor}
                                        onPress={(item) => this._onSelect(item)}
                                    />
                                </Body>
                            </CardItem>

                            <CardItem bordered>
                                <Body>
                                    <Label style={{ flex: 1, marginLeft: 95, color: '#f0ad4e' }}> Description </Label>

                                    <Textarea
                                        rowSpan={5}
                                        bordered placeholder="Please describe your issue clearly in order to provide our best solution for you"
                                        onChangeText={(description) => this.setState({ description })}
                                    />
                                </Body>
                            </CardItem>

                            <CardItem bordered>
                                {/* TouchableOpacity */}
                                <Body>
                                    <Label style={{ flex: 1, marginLeft: 95, color: '#f0ad4e' }}> Upload Image </Label>

                                    <View style={{ flex: 1, flexDirection: 'row' }}>

                                        <TouchableOpacity onPress={() => this.pickSingle()} style={{ marginLeft: 50, marginTop: 10 }}>
                                            <Image source={require('../assets/gallery.png')} />
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={() => this.pickSingleWithCamera(false)} style={{ marginLeft: 100, marginTop: 10 }}>
                                            <Image source={require('../assets/Camera.png')} />
                                        </TouchableOpacity>

                                    </View>


                                </Body>
                            </CardItem>

                            <CardItem bordered>
                                <Body>
                                    {this.state.image ? this.renderAsset(this.state.image) : null}
                                </Body>
                            </CardItem>





                        </Card>
                    </Content>
                    {/* {this.state.images ? this.state.images.map(i => <View style={styles2.iconlImage} key={i.uri}>{this.renderAsset(i)}</View>) : null} */}
                </ScrollView>


                {/* </Content> */}

                <Footer style={footerStyles}>
                    <Button transparent style={nextButton} full onPress={() => this.onClickListener('details')}>
                        <Text style={nbc}>NEXT</Text>

                    </Button>


                </Footer>
            </Container>
        );
    }
}


const styles2 = StyleSheet.create({
    Container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },

    footerIcon: {
        width: 5,
        height: 5
    },

    list: {
        flexDirection: "row",
        flexWrap: "wrap"
    },
    grid1Style: {
        marginTop: 6,
        marginLeft: -0.6

    },

    button: {
        width: 250,
        height: 50,
        backgroundColor: '#330066',
        borderRadius: 30,
        justifyContent: 'center',
        marginTop: 15
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center'
    },
    uploadAvatar: {
        width: 300,
        height: 300,
        marginTop: 20,
        marginLeft: 30
    }
});

const styles = {
    /*
    headerStyles and headerStyle are both use for the header color.
    */


    headerStyles: {
        backgroundColor: '#f0ad4e',
        color: '#ffffff',
        fontSize: 18
    },

    headerStyle: {
        backgroundColor: '#f0ad4e'
    },
    btnStyle2: {
        width: 119,
        height: 100,
        // backgroundColor: '#0693e3'

    },

    btnImage: {
        marginTop: 8,
        width: 300,
        marginLeft: 26,
        marginBottom: 8
    },

    footerStyles: {
        backgroundColor: '#f0ad4e',
        alignItems: 'center'
    },
    progressStyle: {
        height: 4,
        marginTop: 0,
        backgroundColor: 'black',
        width: 300

    },

    headerText: {
        color: 'white'
    },
    areaTopic: {
        fontWeight: 'bold'


    },

    timeStyles: {
        fontSize: 14,
        color: 'black'
    },

    grid1Style: {
        marginTop: 6,
        marginLeft: -0.6

    },

    subStyle: {
        color: 'black',
        fontSize: 12
    },
    datePickerStyle: {
        width: 200,
        backgroundColor: 'black',
        marginLeft: 200

    },


    bodyStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 60

    },

    areaStyles: {
        fontSize: 13,
    },

    nextButton: {
        width: 300,
        height: 55,
        alignItems: 'center',
        backgroundColor: '#f0ad4e',

    },

    nbc: {
        color: 'black',
        fontSize: 16
    }

}