import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";

export default {
    FlashMessage(name, desc, icon, backgroundColor, textColor){
        showMessage({
            message: name,
            description: desc,
            icon: icon,
            backgroundColor: backgroundColor, // background color
            color: textColor, // text color
        })
    }
}
