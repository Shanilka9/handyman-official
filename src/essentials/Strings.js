export default{
  BaseUrl :  'http://5.189.148.181:8080/handyman/',

  AlertIcons: ['none','success','warning','info','danger','default'],

  LogoutMesseage : 'Signing Out',
  LogoutDescription : 'You Have SignOut Successfully!',

  SigningUpAlert: "Signup In Progress",
  SigningUpAlertDescription:"Verify Your Phone Before Signin",

  LoginFailedAlert:"Login Failed",
  LoginFailedAlertDescription: "Username and Password is Incorrect !",

  ConnectionErrorMessage : 'Connection Failed',
  ConnectionErrorDescription : 'You are not connect to the internet',

  RequestFailedAlert : 'Something Went Wrong',
  RequestFailedDescription : 'You request failed to reach the server',

  RequestSuccessAlert : 'Success',
  RequestSuccessDescription : 'Your Request Was Sent Successfully!',

  PrividerNameAlert:"Provider Name Field Empty",
  ProviderNameDescription:"Please Enter Provider Name",

  AddressFieldAlert:"Address Field Empty",
  AddressDescription:"Please Enter Address",

  IssueMobileNumber: "Issue in Mobile Number Field",
  IssueMobileNumberDescription:"Enter Mobile Number Correctly",

  IssueEmail: "Issue in Email Field",
  IssueEmailDescription:"Please Enter Email Correctly",

  NoCategorySelected: "No Category Selected",
  NoCategorySelectedDescription:"Please Select Category",

  NoAreaSelected:"No Area Selected",
  NoAreaSelectedDescription: "Please Select Area",

  ContactPersonNameField:"Contact Person Name Field Empty",
  ContactPersonNameFieldDescription:"Please Enter Contact Person Name",

  IssueContactPersonMobile:"Issue in Contact Person Mobile Field",
  IssueContactPersonMobileDescription:"Please Enter Contact Person Mobile Correctly",

  SelectdateAlert:"Select Date",
  SelectDateDescription:"No Date Selected",

  UploadImage:"Upload Image",
  UploadImageDescription:"No Image Uploaded",

  FullNameEmptyAlert:"Fullname Field Empty",
  FullNameEmptyAlertDescription:"Please Enter FullName",

  UserNameFieldAlert:"Username Field Empty",
  UserNameFieldAlertDescription:"Please Enter Username",

  IssuePasswordAlert:"Issue in Password Field",
  IssuePasswordAlertDescription: "Password should be at least 5 charactors",

  SuccessAlert:"Sign up in Progress",
  SuccessAlertDescription: "We sent a verification code to your phone",

  UserNameAlreadyExistAlert: "Username Already Exist",
  UserNameAlreadyExistAlertDescription: "Please Change Your Username",


  LoginWithPincodeFailedAlert:"Login Failed!",
  LoginWithPincodeFailedDescription:"Pin Code Didn't Match",

  LoginSuccesswithPinCode:"Login Success!",
  LoginSuccesswithPinCodeDescription: "Pin Code Matched",

  ProviderNameFieldAlert:"Provider Name Field Empty",
  PrividerNameAlertDescription:"Please Enter Provider Name",

  pinCodeMatchedAlert:"Success!",
  pinCodeMatchedAlertDescription:"Pin Code Matched",

  pinCodeNotFoundAlert:"Pin Code Error!",
  pinCodeNotFoundAlertDescription:"Enter the Correct Pin Code",

  usernameMatchedAlert: "Success!",
  usernameMatchedAlertDescription:"Username Exsist",

  usernameNotFoundAlert:"Failed!",
  usernameNotFoundAlertDescription:"Username not Exsist",

  passwordResetAlert:"Success!",
  passwordResetAlertDescription: "Password Changed Successfully",

  passwordMismatchAlert:"Password Mismatch!",
  passwordMismatchAlertDescription:"Re-Enter the New Password Correctly",

  noJobListAlert:"Job Lists Empty",
  noJobListAlertDescription:"No Completed Jobs to Display",

  
  
}
