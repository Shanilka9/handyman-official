export default {

    SuccessColor :  '#05661a',
    ErrorColor : '#ed1212',
    AlertTextColor: '#ffffff',
    WarningTextColor: '#000000',
    WarningAlertbackgroundColor: "#f4c741",
    
    nativeColor : "#f0ad4e"

}