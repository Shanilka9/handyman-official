import React, { Component } from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { StyleSheet, View, Picker, ActivityIndicator } from 'react-native';
import { Button, Text } from 'native-base';

export default class DropEx extends Component {



    constructor(props) {
        super(props);
        this.state = {

            pickerData: [],
            pickerSelection: 'Value',
            isLoading: true
        }
    }

    componentWillMount() {
        this.loadPickerItem();
    }

    loadPickerItem() {
        return fetch('https://dash.handyman.qa/api/app/category', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })

            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    pickerData: responseJson
                })
                console.log("respose:>:>:>" + JSON.stringify(this.state.pickerData));

                // console.log("respose >> " + JSON.stringify(responseJson));

                for (var i = 0; i < this.state.pickerData.length; i++) {
                    console.log("categories  " + this.state.categories);
                }
            })

            .catch((error) => {
                console.log(error)
            });
    }

    loadCategoryTypes() {
        return this.state.pickerData.map(data => (
            <Picker.Item label={data.name} value={data.id} />
        ))
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View>
                    <ActivityIndicator />
                </View>
            )
        } else {
            return (

                <View style={{ marginHorizontal: 100, marginVertical: 100 }} >
                    {/* <Text> {this.state.pickerSelection}</Text> */}
                    <Picker
                        style={{ height: 200, width: 200 }}
                        selectedValue={this.state.pickerSelection}
                        onValueChange={(itemValue, itemIndex) => this.setState({ pickerSelection: itemValue })}
                    >
                        {this.loadCategoryTypes()}


                    </Picker>
                </View>








            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 100
    },

    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#848181',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },

});

